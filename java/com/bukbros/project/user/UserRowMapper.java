package com.bukbros.project.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;

public class UserRowMapper implements RowMapper<User>{
	public User mapRow(ResultSet rs, int rowNo) throws SQLException {
		User user = new User();
		user.setId(rs.getInt(UserDatabase.COLUMN_ID));
		user.setUuid(StringFunction.convertTextToUuid(rs.getString(UserDatabase.COLUMN_UUID)));
		user.setUsername(rs.getString(UserDatabase.COLUMN_USERNAME));
		user.setPassword(rs.getString(UserDatabase.COLUMN_PASSWORD));
		user.setEmail(rs.getString(UserDatabase.COLUMN_EMAIL));
		user.setFirstName(rs.getString(UserDatabase.COLUMN_FIRST_NAME));
		user.setLastName(rs.getString(UserDatabase.COLUMN_LAST_NAME));
		user.setAddress(rs.getString(UserDatabase.COLUMN_ADDRESS));
		user.setGender(rs.getByte(UserDatabase.COLUMN_GENDER));
		user.setDob(DateValidation.formatDateFromDatabase(rs.getString(UserDatabase.COLUMN_DOB)));
		user.setPhone(rs.getString(UserDatabase.COLUMN_PHONE));
		user.setImage(rs.getString(UserDatabase.COLUMN_IMAGE));
		user.setRole(rs.getString(UserDatabase.COLUMN_ROLE));
		user.setStatus(rs.getByte(UserDatabase.COLUMN_STATUS));
		user.setCreatedDate(rs.getDate(UserDatabase.COLUMN_CREATED_DATE));
		return user;
	}

}
