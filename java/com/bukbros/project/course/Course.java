package com.bukbros.project.course;

import javax.validation.constraints.NotBlank;

import org.springframework.stereotype.Component;

import com.bukbros.project.function.StringFunction;

@Component
public class Course {
	private int id;
	private String uuid ;
	private String code ;
	
	@NotBlank(message = CourseLanguage.VALIDATION_NOT_EMPTY)
	private String startTime ;
	
	@NotBlank(message = CourseLanguage.VALIDATION_NOT_EMPTY)
	private String endTime ;
	
	public Course() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = StringFunction.trimSpace(code);
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}	
	
}
