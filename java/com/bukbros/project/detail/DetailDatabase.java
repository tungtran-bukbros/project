package com.bukbros.project.detail;

public class DetailDatabase {
	public static final String TABLE = "cr_detail";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_COURSE_ID = "course_id";
	public static final String COLUMN_TERM = "term";
	public static final String COLUMN_SUBJECT_ID = "subject_id";
	public static final String COLUMN_CLASS_ID = "class_id";
	public static final String COLUMN_TTME_ID = "time_id";
	public static final String COLUMN_START_TIME = "start_time";
	public static final String COLUMN_END_TIME = "end_time";
	
	public static final String TABLE_COURSE = "cr_course";
	public static final String TABLE_COURSE_ID = "id";
	public static final String TABLE_COURSE_CODE = "code";
	
	public static final String TABLE_SUBJECT = "cr_subject";
	public static final String TABLE_SUBJECT_ID = "id";
	public static final String TABLE_SUBJECT_TITLE = "title";
	public static final String TABLE_SUBJECT_CODE = "code";
	
	public static final String TABLE_CLASS = "cr_class";
	public static final String TABLE_CLASS_ID = "id";
	public static final String TABLE_CLASS_NAME = "name";
}
