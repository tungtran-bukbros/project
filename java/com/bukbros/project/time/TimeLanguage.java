package com.bukbros.project.time;

import java.util.HashMap;

public class TimeLanguage {
	public static final String VALIDATION_TIME_EXISTS = "Thời khóa biểu đã tồn tại";
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	
	public static final String LABEL_WEEK_DAY = "Thứ";
	public static final String LABEL_BLOCK = "Ca";
	public static final String LABEL_DETAIL_ID = "Detail";
	
	public static final String LABEL_CREATE = "Thêm mới thời gian học";
	public static final String LABEL_UPDATE = "Cập nhật thời gian học";
	public static final String LABEL_LIST = "Danh sách thời gian học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_WEEK_DAY", LABEL_WEEK_DAY);
		labels.put("LABEL_BLOCK", LABEL_BLOCK);
		labels.put("LABEL_DETAIL_ID", LABEL_DETAIL_ID);		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
