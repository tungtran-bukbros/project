package com.bukbros.project.student;

public class StudentDatabase {
	public static final String TABLE = "cr_student";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_USERNAME = "username";
	public static final String COLUMN_PASSWORD = "password";
	public static final String COLUMN_MAJOR_ID = "major_id";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_FIRST_NAME = "first_name";
	public static final String COLUMN_LAST_NAME = "last_name";
	public static final String COLUMN_ADDRESS = "address";
	public static final String COLUMN_DOB = "dob";
	public static final String COLUMN_GENDER = "gender";
	public static final String COLUMN_PHONE = "phone";
	public static final String COLUMN_STATUS = "status";
	public static final String COLUMN_CREATED_DATE = "created_date";
	
	public static final String TABLE_MAJOR = "cr_majors";
	public static final String TABLE_MAJOR_ID = "id";
	public static final String TABLE_MAJOR_CODE = "code";
	public static final String TABLE_MAJOR_NAME = "name";
}
