package com.bukbros.project.faculty;

import java.util.HashMap;

public class FacultyLanguage {
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_NAME = "Tên khoa không hợp lệ";
	public static final String VALIDATION_CODE = "Mã khoa không hợp lệ";
	public static final String VALIDATION_SPECIAL_CHARACTERS = "Không được sử dụng ký tự đặc biệt và số";
	
	public static final String LABEL_NAME = "Tên khoa";
	
	public static final String LABEL_CREATE = "Thêm mới khoa";
	public static final String LABEL_UPDATE = "Cập nhật khoa";
	public static final String LABEL_LIST = "Danh sách khoa";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
