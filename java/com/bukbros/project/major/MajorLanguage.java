package com.bukbros.project.major;

import java.util.HashMap;

public class MajorLanguage {
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_NAME = "Tên ngành học không hợp lệ";
	public static final String VALIDATION_CODE = "Mã ngành học không hợp lệ";
	public static final String VALIDATION_SPECIAL_CHARACTERS = "Không được sử dụng ký tự đặc biệt và số";
	
	public static final String LABEL_NAME = "Tên ngành học";
	public static final String LABEL_CODE = "Mã ngành";
	
	public static final String LABEL_CREATE = "Thêm mới ngành học";
	public static final String LABEL_UPDATE = "Cập nhật ngành học";
	public static final String LABEL_LIST = "Danh sách ngành học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_CODE", LABEL_CODE);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
