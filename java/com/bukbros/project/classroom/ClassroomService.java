package com.bukbros.project.classroom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.project.function.StringFunction;

@Service
public class ClassroomService {
	@Autowired
	private ClassroomRepository repository;
	
	public List<Classroom> getAllClassroom() {
		return repository.read();
	}
	
	public boolean create(Classroom classroom) {
		classroom.setSlug(StringFunction.convertTextToSlug(classroom.getName()));
		boolean validSlug = repository.isSlugExists(classroom.getSlug());
		if (validSlug) {
			classroom.setSlug(StringFunction.generateSlug(classroom.getName()));
		}
		return repository.create(classroom);
	}
	
	public boolean delete(Classroom classroom) {
		return repository.delete(classroom.getId());
	}
	
	public boolean update(int id, Classroom classroom) {
		return repository.update(id, classroom);
	}
	
	public Classroom getClassroomByUuid (String uuid) {
		return repository.getClassroomByUuid(uuid);
	}
	
	public boolean isSlugExists(String slug) {
		return repository.isSlugExists(slug);
	}
	
	public boolean addClassSubject (int subjectId, int classId) {
		return addClassSubject(subjectId, classId);
	}
}
