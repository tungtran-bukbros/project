package com.bukbros.project.staff;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class StaffRowMapper implements RowMapper<Staff>{
	public Staff mapRow(ResultSet rs, int rowNo) throws SQLException {
		Staff staff = new Staff();
		staff.setUserId(rs.getInt(StaffDatabase.COLUMN_USER_ID));
		staff.setFacultyId(rs.getInt(StaffDatabase.COLUMN_FACULTY_ID));
		staff.setCertificate(rs.getString(StaffDatabase.COLUMN_CERTIFICATE));
		return staff;		
	}
}
