<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row mb-3">
	<div class="col-sm-3">
		<c:choose>
			<c:when test="${user.image == null}">
				<img src="<c:url value="/public/images/avt_null.png" />" border="0" width="100%">
			</c:when>
			<c:otherwise>
				<img src="<c:url value="${user.image}" />" border="0" width="100%">
			</c:otherwise>
		</c:choose>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Username : ${user.username}</li>
			<li class="list-group-item">Họ tên : ${user.firstName.concat(" ").concat(user.lastName)}</li>
			<li class="list-group-item">Khoa : ${user.facultyName}</li>
			<li class="list-group-item">Email : ${user.email}</li>
			<li class="list-group-item">Địa chỉ : ${user.address}</li>		
		</ul>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Ngày sinh : ${user.dob}</li>	
			<li class="list-group-item">Giới tính : ${user.gender == 1 ? 'Nam' : 'Nữ'}</li>
			<li class="list-group-item">Số điện thoại : ${user.phone}</li>
			<li class="list-group-item">Chứng chỉ đạt được : ${user.certificate}</li>
		</ul>
	</div>	
</div>
 