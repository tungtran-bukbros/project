package com.bukbros.project.time;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class TimeRepository implements RepositoryInterface<Time>{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean create(Time time) {
		String sql = "INSERT INTO " + TimeDatabase.TABLE
					+ " ("
					+ TimeDatabase.COLUMN_DETAIL_ID+ ", "
					+ TimeDatabase.COLUMN_WEEK_DAY+ ", "
					+ TimeDatabase.COLUMN_BLOCK+ ", "
					+ TimeDatabase.COLUMN_STATUS
					+ ") VALUES (?,?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     time.getDetailId(),
		                                     time.getWeekDay(),
		                                     time.getBlock(),
		                                     time.getStatus());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Time> read() {
		String sql = "SELECT * FROM " + TimeDatabase.TABLE 
				   + " ORDER BY " + TimeDatabase.COLUMN_DETAIL_ID + " ASC";
		List<Time> times = jdbcTemplate.query(sql, new TimeRowMapper());
		if (times.isEmpty()) {
			return null;
		}
		return times;
	}

	@Override
	public boolean update(int detailId, Time time) {
		String sql ="UPDATE " + TimeDatabase.TABLE
					+ " SET " + TimeDatabase.COLUMN_BLOCK+ " = ?, "
					+ TimeDatabase.COLUMN_STATUS+ " = ?"
					+ " WHERE " + TimeDatabase.COLUMN_DETAIL_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                     time.getBlock(),
		                                     time.getStatus(),
		                                     detailId);
		if (rowUpdated > 0) {
			return true;
		}                   
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean status(int id, int status) {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean status(int detailId, String weekDay, int status) {
		try {
			String sql = "UPDATE " + TimeDatabase.TABLE 
		               + " SET " + TimeDatabase.COLUMN_STATUS + " = ? " 
		               + "WHERE " + TimeDatabase.COLUMN_DETAIL_ID + " = ? "
		               + " AND " + TimeDatabase.COLUMN_WEEK_DAY + " = ?";
			int rowActive = jdbcTemplate.update(sql, status, detailId, weekDay);
			return (rowActive > 0) ? true : false;  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected boolean deleteBlock (int detailId, String weekDay) {
		String sql = " DELETE FROM " + TimeDatabase.TABLE
					+ " WHERE " + TimeDatabase.COLUMN_DETAIL_ID+ " =? "
					+ " AND " + TimeDatabase.COLUMN_WEEK_DAY+ " =? ";
		int rowDeleted = jdbcTemplate.update(sql, detailId, weekDay);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}
	
	protected Time isTimeExists(int detailId, String weekDay, String block) {
		Time time;
		try {
			String sql = "SELECT * FROM " + TimeDatabase.TABLE
								   + " WHERE " + TimeDatabase.COLUMN_DETAIL_ID + " = ?"
								   + " AND " + TimeDatabase.COLUMN_WEEK_DAY+ " = ?"
								   + " AND " + TimeDatabase.COLUMN_BLOCK+ " = ?";
			 time = jdbcTemplate.queryForObject(sql, new Object[] { detailId, weekDay, block }, new TimeRowMapper());			
		} catch (Exception e) {
			return null;
		}
		return time;
	}
	
	protected List<Time> groupByDetailId () {
			String sql ="SELECT " + TimeDatabase.COLUMN_DETAIL_ID+ " , " 
					+ TimeDatabase.COLUMN_WEEK_DAY+ " , " 
					+ TimeDatabase.COLUMN_STATUS
					+ " FROM " + TimeDatabase.TABLE
					+ " GROUP BY " + TimeDatabase.COLUMN_DETAIL_ID+ " , "
					+ TimeDatabase.COLUMN_WEEK_DAY;
		List<Time> time = jdbcTemplate.query(sql, new TimeGroupByRowMapper());
		if (time.isEmpty()) {
			return null;
		}
		return time;		
	}
	
	protected List<String> getBlocksByDetailIdWeekDay (int detailId, String weekDay) {
			String sql = "SELECT " + TimeDatabase.COLUMN_BLOCK
						+ " FROM " + TimeDatabase.TABLE
						+ " WHERE " + TimeDatabase.COLUMN_DETAIL_ID+ " = ? "
						+ " AND " + TimeDatabase.COLUMN_WEEK_DAY+ " = ? ";
		List<String> time = jdbcTemplate.queryForList(sql, new Object[] { detailId, weekDay }, String.class);
		if (time.isEmpty()) {
			return null;
		}
		return time;
	}
	
	protected List<Integer> getWeekDaysByDetailId (int detailId){
		String sql = " SELECT " + TimeDatabase.COLUMN_WEEK_DAY
				+ " FROM " + TimeDatabase.TABLE
				+ " WHERE " + TimeDatabase.COLUMN_DETAIL_ID+ " = ? ";
		List<Integer> weekDays = jdbcTemplate.queryForList(sql, new Object[] { detailId }, Integer.class);
		if (weekDays.isEmpty()) {
			return null;
		}
		return weekDays;
	}

	
}
