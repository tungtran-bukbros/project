package com.bukbros.project.major;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Role;
import com.bukbros.project.configuration.Url;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.user.User;
import com.bukbros.project.user.UserService;

@Controller
@RequestMapping(value = MajorConfig.MODULE_ADMIN_URL)
public class MajorController {
	@Autowired
	private MajorService service;
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return MajorConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return MajorLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAllMajor(Model model, Principal principal) {
		model.addAttribute("listMode", true);
		User currentUser = userService.getUserByUsername(principal.getName());
		List<Major> majors = service.getAllMajor();
		if (majors == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("majors", majors);					
		}
		model.addAttribute("staff", currentUser);	
		return MajorConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (Model model,
	                         Principal principal,
							 RedirectAttributes redirectAttributes) {
		User currentUser = userService.getUserByUsername(principal.getName());
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("createMode", true);
		return MajorConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validMajor") @Valid Major major,
							  BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("major", major);
		if(results.hasErrors()) {
			return MajorConfig.MODULE_VIEW_ADMIN;
		}else {
			boolean validCode = service.isCodeExists(major.getCode());
			if (validCode) {
				results.addError(new FieldError("validMajor", "code", MajorLanguage.VALIDATION_CODE));
			}
			boolean validName = service.isNameExists(major.getName());
			if (validName) {
				results.addError(new FieldError("validMajor", "name", MajorLanguage.VALIDATION_NAME));
			}
			if (results.hasFieldErrors("code")	|| results.hasFieldErrors("name")) {
				return MajorConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(major);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate (@PathVariable("uuid") String uuid,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		Major originalMajor = service.getMajorByUuid(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("major", originalMajor);
		return MajorConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
	                          @ModelAttribute("validMajor") @Valid Major major,
	                          BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		Major originalMajor = service.getMajorByUuid(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("major", major);
		if (results.hasErrors()) {
			return MajorConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!major.getCode().equalsIgnoreCase(originalMajor.getCode())) {
				boolean validCode = service.isCodeExists(major.getCode());
				if (validCode) {
					results.addError(new FieldError("validMajor", "code", MajorLanguage.VALIDATION_CODE));
				}
			}
			if (!major.getName().equalsIgnoreCase(originalMajor.getName())) {
				boolean validName = service.isNameExists(major.getName());
				if (validName) {
					results.addError(new FieldError("validMajor", "name", MajorLanguage.VALIDATION_NAME));
				}
			}
			if (results.hasFieldErrors("code")	|| results.hasFieldErrors("name")) {
				return MajorConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalMajor.getId(), major)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String activeMajor(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Major originalMajor = service.getMajorByUuid(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean active = service.status(originalMajor.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveMajor(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Major originalMajor = service.getMajorByUuid(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean deactive = service.status(originalMajor.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE )
	public String deleteSubject(@PathVariable("uuid") String uuid,
	                            Model model,
	                            RedirectAttributes redirectAttributes) {
		Major originalMajor = service.getMajorByUuid(uuid);
		if (originalMajor == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_MAJOR;
		}
		boolean delete = service.delete(originalMajor);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_FAILURE);
		}
		return "redirect:" + Url.URL_MAJOR;
	}
}
