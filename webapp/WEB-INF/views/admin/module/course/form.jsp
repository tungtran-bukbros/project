<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value='${urlGlobals.URL_COURSE}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
	</div>
</div>
<hr class="my-3">
<c:url var="urlForm" value="${urlGlobals.URL_COURSE.concat(urlGlobals.URL_CREATE)}"></c:url>
<form:form action="${urlForm}" method="post" modelAttribute="validCourse">
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Khóa học</label>
		<div class="col-sm-7">
			<input type="text" name="code" class="form-control" value="${course.code}" path="code">
			<form:errors path="code" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày bắt đầu</label>
		<div class="col-sm-7">
			<input type="text" class="form-control datepicker-top" name="startTime" value="${course.startTime}" placeholder="Định dạng : ngày/tháng/năm"
				path="startTime">
			<form:errors path="startTime" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày kết thúc</label>
		<div class="col-sm-7">
			<input type="text" class="form-control datepicker-top" name="endTime" value="${course.endTime}" placeholder="Định dạng : ngày/tháng/năm"
				path="endTime">
			<form:errors path="endTime" cssClass="error" />
		</div>
	</div>
	<hr />
	<div class="form-group row">
		<div class="col-sm-9 offset-sm-3">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
	</div>
</form:form>