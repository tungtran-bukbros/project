package com.bukbros.project.staff;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;

public class StaffFullRowMapper implements RowMapper<Staff>{
	public Staff mapRow(ResultSet rs, int rowNo) throws SQLException {
		Staff staff = new Staff();
		staff.setUserId(rs.getInt(StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID));
		staff.setFacultyId(rs.getInt(StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID));
		staff.setCertificate(rs.getString(StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_CERTIFICATE));
		staff.setId(rs.getInt(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID));
		staff.setUuid(StringFunction.convertTextToUuid(rs.getString(StaffDatabase.TABLE_USER_UUID)));
		staff.setUsername(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_USERNAME));
		staff.setPassword(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PASSWORD));
		staff.setEmail(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_EMAIL));
		staff.setFirstName(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_FIRST_NAME));
		staff.setLastName(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_LAST_NAME));
		staff.setAddress(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ADDRESS));
		staff.setGender(rs.getByte(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_GENDER));
		staff.setDob(DateValidation.formatDateFromDatabase(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_DOB)));
		staff.setPhone(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PHONE));
		staff.setImage(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_IMAGE));
		staff.setRole(rs.getString(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ROLE));
		staff.setStatus(rs.getByte(StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_STATUS));
		staff.setFacultyName(rs.getString(StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_NAME));
		return staff;		
	}

}
