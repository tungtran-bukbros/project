package com.bukbros.project.configuration;

import java.util.HashMap;

import org.springframework.beans.TypeMismatchException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.NoHandlerFoundException;


@ControllerAdvice
public class GlobalExceptionHandler {

	@ModelAttribute("urlGlobals")
	public HashMap<String, String> home() {
		return Url.urlGlobal();
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public String notFound() {
		return "redirect:" + Url.URL_ERROR_NOT_FOUND;
	}

	@ExceptionHandler(TypeMismatchException.class)
	public String handleBadParameters() {
		return "redirect:" + Url.URL_ADMIN + Url.URL_ERROR_NOT_FOUND;
	}
}
