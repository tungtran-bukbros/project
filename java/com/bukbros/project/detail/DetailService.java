package com.bukbros.project.detail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetailService {
	@Autowired
	private DetailRepository repository;
	
	public boolean create(Detail detail) {
		return repository.create(detail);
	}
	
	public List<Detail> getAllDetail() {
		return repository.getAllDetail();
	}
	
	public boolean update(int id, Detail detail) {
		return repository.update(id, detail);
	}
	
	public Detail getDetailByUuid (String uuid) {
		return repository.getDetailByUuid(uuid);
	}
	
	public Detail getDetailById (int id) {
		return repository.getDetailById(id);
	}
	
	public Detail isDetailExists (int courseId, int term, int subjectId, int classId) {
		return repository.isDetailExists(courseId, term, subjectId, classId);
	}
}
