package com.bukbros.project.course;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService {
	@Autowired
	private CourseRepository repository;
	
	public boolean create(Course course) {
		return repository.create(course);
	}
	
	public List<Course> getAllCourse() {
		return repository.read();
	}
	
	public boolean isCodeExists(String code) {
		return repository.isCodeExists(code);
	}	
}
