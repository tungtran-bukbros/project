package com.bukbros.project.detail;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.interfaces.RepositoryInterface;
@Repository
public class DetailRepository implements RepositoryInterface<Detail>{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean create(Detail detail) {
		String sql = "INSERT INTO " + DetailDatabase.TABLE
					+ " ("
					+ DetailDatabase.COLUMN_UUID+ ", "
					+ DetailDatabase.COLUMN_COURSE_ID+ ", "
					+ DetailDatabase.COLUMN_TERM+ ", "
					+ DetailDatabase.COLUMN_SUBJECT_ID+ ", "
					+ DetailDatabase.COLUMN_CLASS_ID+ ", "
					+ DetailDatabase.COLUMN_START_TIME+ ", "
					+ DetailDatabase.COLUMN_END_TIME					
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?,?,?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     detail.getCourseId(),
		                                     detail.getTerm(),
		                                     detail.getSubjectId(),
		                                     detail.getClassId(),
		                                     DateValidation.formatDate(detail.getStartTime()),
		                                     DateValidation.formatDate(detail.getEndTime()));
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Detail> read() {
		String sql = "SELECT " + DetailDatabase.COLUMN_ID + ", "
				   + "HEX(" + DetailDatabase.COLUMN_UUID + ") as " + DetailDatabase.COLUMN_UUID + ", "
				   + DetailDatabase.COLUMN_COURSE_ID + ", "
				   + DetailDatabase.COLUMN_TERM + ", "
				   + DetailDatabase.COLUMN_SUBJECT_ID + ", "
				   + DetailDatabase.COLUMN_CLASS_ID + ", "
				   + DetailDatabase.COLUMN_START_TIME + ", "
				   + DetailDatabase.COLUMN_END_TIME
				   + " FROM " + DetailDatabase.TABLE 
				   + " ORDER BY " + DetailDatabase.COLUMN_ID + " ASC";
		List<Detail> details = jdbcTemplate.query(sql, new DetailRowMapper());
		if (details.isEmpty()) {
			return null;
		}
		return details;
	}

	@Override
	public boolean update(int id, Detail detail) {
		String sql = "UPDATE " + DetailDatabase.TABLE
					+ " SET " + DetailDatabase.COLUMN_COURSE_ID+ " = ?, "
					+ DetailDatabase.COLUMN_TERM+ " = ?, "
					+ DetailDatabase.COLUMN_SUBJECT_ID+ " = ?, "
					+ DetailDatabase.COLUMN_CLASS_ID+ " = ?, "
					+ DetailDatabase.COLUMN_START_TIME+ " = ?, "
					+ DetailDatabase.COLUMN_END_TIME+ " = ? "
					+ "WHERE "+ DetailDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                     detail.getCourseId(),
		                                     detail.getTerm(),
		                                     detail.getSubjectId(),
		                                     detail.getClassId(),
		                                     DateValidation.formatDate(detail.getStartTime()),
		                                     DateValidation.formatDate(detail.getEndTime()),
		                                     id);
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean status(int id, int status) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected List<Detail> getAllDetail() {
		String sql = "SELECT " + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_ID + ", "
				   + "HEX(" + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_UUID + ") as " + DetailDatabase.COLUMN_UUID + ", "
				   + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_COURSE_ID + ", "
				   + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_TERM + ", "
				   + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_SUBJECT_ID + ", "
				   + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_CLASS_ID + ", "
				   + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_START_TIME + ", "
				   + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_END_TIME + ", "
				   + DetailDatabase.TABLE_COURSE + "." + DetailDatabase.TABLE_COURSE_CODE + ", "
				   + DetailDatabase.TABLE_CLASS + "." + DetailDatabase.TABLE_CLASS_NAME + ", "
				   + DetailDatabase.TABLE_SUBJECT + "." + DetailDatabase.TABLE_SUBJECT_TITLE + ", "
				   + DetailDatabase.TABLE_SUBJECT + "." + DetailDatabase.TABLE_SUBJECT_CODE
				   + " FROM " + DetailDatabase.TABLE
				   + " INNER JOIN " + DetailDatabase.TABLE_COURSE
				   + " ON " + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_COURSE_ID + " = " + DetailDatabase.TABLE_COURSE + " . " + DetailDatabase.TABLE_COURSE_ID
				   + " INNER JOIN " + DetailDatabase.TABLE_CLASS
				   + " ON " + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_CLASS_ID + " = " + DetailDatabase.TABLE_CLASS + " . " + DetailDatabase.TABLE_CLASS_ID
				   + " INNER JOIN " + DetailDatabase.TABLE_SUBJECT
				   + " ON " + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_SUBJECT_ID + " = " + DetailDatabase.TABLE_SUBJECT + " . " + DetailDatabase.TABLE_SUBJECT_ID
				   + " ORDER BY " + DetailDatabase.TABLE + "." + DetailDatabase.COLUMN_ID + " ASC";
		List<Detail> details = jdbcTemplate.query(sql, new DetailFullRowMapper());
		if (details.isEmpty()) {
			return null;
		}
		return details;
	}
	
	protected Detail getDetailByUuid (String uuid) {
		Detail detail;
		try {
			String sql = " SELECT " + DetailDatabase.COLUMN_ID + ", "
								+ "HEX(" + DetailDatabase.COLUMN_UUID + ") as " + DetailDatabase.COLUMN_UUID+ ", "								
							    + DetailDatabase.COLUMN_COURSE_ID + ", "
							    + DetailDatabase.COLUMN_TERM + ", "
							    + DetailDatabase.COLUMN_SUBJECT_ID + ", "
							    + DetailDatabase.COLUMN_CLASS_ID + ", "
							    + DetailDatabase.COLUMN_START_TIME + ", "
							    + DetailDatabase.COLUMN_END_TIME
								+ " FROM " + DetailDatabase.TABLE
								+ " WHERE " + DetailDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			detail = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new DetailRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return detail;
	}	
	
	protected Detail getDetailById (int id) {
		Detail detail;
		try {
			String sql = " SELECT " + DetailDatabase.COLUMN_ID + ", "
								+ "HEX(" + DetailDatabase.COLUMN_UUID + ") as " + DetailDatabase.COLUMN_UUID+ ", "								
							    + DetailDatabase.COLUMN_COURSE_ID + ", "
							    + DetailDatabase.COLUMN_TERM + ", "
							    + DetailDatabase.COLUMN_SUBJECT_ID + ", "
							    + DetailDatabase.COLUMN_CLASS_ID + ", "
							    + DetailDatabase.COLUMN_START_TIME + ", "
							    + DetailDatabase.COLUMN_END_TIME
								+ " FROM " + DetailDatabase.TABLE
								+ " WHERE " + DetailDatabase.COLUMN_ID + " = ?";
			detail = jdbcTemplate.queryForObject(sql, new Object[] { id }, new DetailRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return detail;
	}	
	
	protected Detail isDetailExists (int courseId, int term, int subjectId, int classId) {
		Detail detail;
		try {
			String sql = "SELECT " + DetailDatabase.COLUMN_ID + ", "
					   + "HEX(" + DetailDatabase.COLUMN_UUID + ") as " + DetailDatabase.COLUMN_UUID + ", "
					   + DetailDatabase.COLUMN_COURSE_ID + ", "
					   + DetailDatabase.COLUMN_TERM + ", "
					   + DetailDatabase.COLUMN_SUBJECT_ID + ", "
					   + DetailDatabase.COLUMN_CLASS_ID + ", "
					   + DetailDatabase.COLUMN_START_TIME + ", "
					   + DetailDatabase.COLUMN_END_TIME
					   + " FROM " + DetailDatabase.TABLE 
					   + " WHERE "  + DetailDatabase.COLUMN_COURSE_ID+ " = ? "
					   + " AND " + DetailDatabase.COLUMN_TERM+ " = ? "
					   + " AND " + DetailDatabase.COLUMN_SUBJECT_ID+ " = ? "
					   + " AND " + DetailDatabase.COLUMN_CLASS_ID+ " = ? ";
				detail = jdbcTemplate.queryForObject(sql, new Object[] { courseId, term, subjectId, classId }, new DetailRowMapper());			
		} catch (Exception e) {
			return null;
		}
			
		return detail;
	}
	
}
