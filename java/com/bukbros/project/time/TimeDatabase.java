package com.bukbros.project.time;

public class TimeDatabase {
	public static final String TABLE = "cr_time";
	public static final String COLUMN_DETAIL_ID = "detail_id";
	public static final String COLUMN_WEEK_DAY = "week_day";
	public static final String COLUMN_BLOCK = "block";
	public static final String COLUMN_STATUS = "status";
}
