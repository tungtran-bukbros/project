<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value='${urlGlobals.URL_CLASSROOM.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i> 
		${labelLang.LABEL_BUTTON_CREATE}
		</a>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_NAME}</th>
			<th class="text-center">${moduleLang.LABEL_SLUG}</th>
			<th class="text-center">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${classrooms}" var="classroom" varStatus="loop">
			<tr>
				<td class="text-center"></td>
				<td class="text-center">${classroom.name}</td>
				<td class="text-center">${classroom.slug}</td>
				<td class="text-center">
					<a href='<c:url value="${urlGlobals.URL_CLASSROOM.concat('/').concat(classroom.uuid).concat(urlGlobals.URL_UPDATE)}" />'
						class="btn btn-primary btn-sm"> ${labelLang.LABEL_BUTTON_UPDATE} </a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>