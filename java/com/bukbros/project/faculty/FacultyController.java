package com.bukbros.project.faculty;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Role;
import com.bukbros.project.configuration.Url;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.user.User;
import com.bukbros.project.user.UserService;

@Controller
@RequestMapping(value = FacultyConfig.MODULE_ADMIN_URL)
public class FacultyController {
	@Autowired
	private FacultyService service;
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return FacultyConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return FacultyLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAllFaculty(Model model, Principal principal) {
		model.addAttribute("listMode", true);
		User currentUser = userService.getUserByUsername(principal.getName());
		List<Faculty> faculties = service.getAllFaculty();
		if (faculties == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("faculties", faculties);					
		}
		model.addAttribute("staff", currentUser);
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (Model model,
	                         Principal principal,
							 RedirectAttributes redirectAttributes) {
		User currentUser = userService.getUserByUsername(principal.getName());
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
			return "redirect:" + Url.URL_FACULTY;
		}
		model.addAttribute("createMode", true);
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validFaculty") @Valid Faculty faculty,
							  BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("faculty", faculty);
		if(results.hasErrors()) {
			return FacultyConfig.MODULE_VIEW_ADMIN;
		}else {
			boolean validName = service.isNameExists(faculty.getName());
			if (validName) {
				results.addError(new FieldError("validFaculty", "name", FacultyLanguage.VALIDATION_NAME));
			}
			if (results.hasFieldErrors("name")) {
				return FacultyConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(faculty);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_FACULTY;
	}
	
	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate (@PathVariable("uuid") String uuid,
	                         Principal principal,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		Faculty originalFaculty = service.getFacultyByUuid(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		User currentUser = userService.getUserByUsername(principal.getName());
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
			return "redirect:" + Url.URL_FACULTY;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("major", originalFaculty);
		return FacultyConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
	                          @ModelAttribute("validFaculty") @Valid Faculty faculty,
	                          BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		Faculty originalFaculty = service.getFacultyByUuid(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("faculty", faculty);
		if (results.hasErrors()) {
			return FacultyConfig.MODULE_VIEW_ADMIN;
		} else {			
			if (!faculty.getName().equalsIgnoreCase(originalFaculty.getName())) {
				boolean validName = service.isNameExists(faculty.getName());
				if (validName) {
					results.addError(new FieldError("validFaculty", "name", FacultyLanguage.VALIDATION_NAME));
				}
			}
			if (results.hasFieldErrors("name")) {
				return FacultyConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalFaculty.getId(), faculty)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_FACULTY;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String activeFaculty(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Faculty originalFaculty = service.getFacultyByUuid(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		boolean active = service.status(originalFaculty.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_FACULTY;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveFaculty(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Faculty originalFaculty = service.getFacultyByUuid(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		boolean deactive = service.status(originalFaculty.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_FACULTY;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE )
	public String deleteSubject(@PathVariable("uuid") String uuid,
	                            Model model,
	                            RedirectAttributes redirectAttributes) {
		Faculty originalFaculty = service.getFacultyByUuid(uuid);
		if (originalFaculty == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_FACULTY;
		}
		boolean delete = service.delete(originalFaculty);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_FAILURE);
		}
		return "redirect:" + Url.URL_FACULTY;
	}

}
