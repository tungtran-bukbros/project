package com.bukbros.project.detail;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;

public class DetailRowMapper implements RowMapper<Detail>{
	public Detail mapRow(ResultSet rs, int rowNo) throws SQLException {
		Detail detail = new Detail();
		detail.setId(rs.getInt(DetailDatabase.COLUMN_ID));
		detail.setUuid(StringFunction.convertTextToUuid(rs.getString(DetailDatabase	.COLUMN_UUID)));
		detail.setCourseId(rs.getInt(DetailDatabase.COLUMN_COURSE_ID));
		detail.setTerm(rs.getInt(DetailDatabase.COLUMN_TERM));
		detail.setSubjectId(rs.getInt(DetailDatabase.COLUMN_SUBJECT_ID));
		detail.setClassId(rs.getInt(DetailDatabase.COLUMN_CLASS_ID));
		detail.setStartTime(DateValidation.formatDateFromDatabase(rs.getString(DetailDatabase.COLUMN_START_TIME)));
		detail.setEndTime(DateValidation.formatDateFromDatabase(rs.getString(DetailDatabase.COLUMN_END_TIME)));	
		return detail;
	}

}
