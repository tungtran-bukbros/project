<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<h5 class="text-uppercase mt-1 mb-0">
	<i class="fas fa-edit"></i> Đổi mật khẩu
</h5>
<hr class="my-3">
<c:url var="urlForm" value="${urlGlobals.URL_STUDENT.concat('/').concat(student.uuid).concat(urlGlobals.URL_CHANGE_PASSWORD)}" />
<form:form action="${urlForm}" method="post" modelAttribute="changePassword">
	<!-- Old password -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Mật khẩu cũ <span class="text-danger"> *</span>
		</label>
		<div class="col-sm-7">
			<input type="password" class="form-control" name="oldPassword" path="oldPassword">
			<form:errors path="oldPassword" cssClass="error" />
		</div>
	</div>
	<!-- New password -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Mật khẩu mới <span class="text-danger"> *</span>
		</label>
		<div class="col-sm-7">
			<input type="password" class="form-control" name="password" path="password">
			<form:errors path="password" cssClass="error" />
		</div>
	</div>
	<!-- Confirm password -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Nhập lại mật khẩu <span class="text-danger"> *</span>
		</label>
		<div class="col-sm-7">
			<input type="password" class="form-control" name="confirmPassword" path="confirmPassword">
			<form:errors path="confirmPassword" cssClass="error" />
		</div>
	</div>
	<!--Button submit -->
	<div class="form-group row">
		<div class="offset-sm-3 col-sm-7">
			<hr>
			<button type="submit" class="btn btn-primary">
				<i class="fas fa-edit"></i> Cập nhập
			</button>
		</div>
	</div>
</form:form>