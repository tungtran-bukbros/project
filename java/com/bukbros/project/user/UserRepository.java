package com.bukbros.project.user;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;
import com.bukbros.project.interfaces.RepositoryInterface;
@Repository
public class UserRepository implements RepositoryInterface<User>{
	@Autowired
	private JdbcTemplate jdbcTemplate;	

	@Override
	public boolean create(User user) {
		String sql = "INSERT INTO " + UserDatabase.TABLE
					+ " ("
					+ UserDatabase.COLUMN_UUID+ ", "
					+ UserDatabase.COLUMN_USERNAME+ ", "
					+ UserDatabase.COLUMN_PASSWORD+ ", "
					+ UserDatabase.COLUMN_EMAIL+ ", "
					+ UserDatabase.COLUMN_FIRST_NAME+ ", "
					+ UserDatabase.COLUMN_LAST_NAME+ ", "
					+ UserDatabase.COLUMN_ADDRESS+ ", "
					+ UserDatabase.COLUMN_GENDER+ ", "
					+ UserDatabase.COLUMN_DOB+ ", "
					+ UserDatabase.COLUMN_PHONE+ ", "
					+ UserDatabase.COLUMN_IMAGE+ ", "
					+ UserDatabase.COLUMN_ROLE+ ", "
					+ UserDatabase.COLUMN_STATUS
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?,?,?,?,?,?,?,?,?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     user.getUsername(),
		                                     UserFunction.encoderPassword(StringFunction.trimSpace(user.getPassword())),
		                                     user.getEmail(),
		                                     user.getFirstName(),
		                                     user.getLastName(),
		                                     user.getAddress(),
		                                     user.getGender(),
		                                     DateValidation.formatDate(user.getDob()),
		                                     user.getPhone(),
		                                     user.getImage(),
		                                     user.getRole(),
		                                     user.getStatus());
		
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}
	
	@Override
	public List<User> read() {
			String sql = "SELECT " + UserDatabase.COLUMN_ID + ", "
								   + "HEX(" + UserDatabase.COLUMN_UUID + ") as " + UserDatabase.COLUMN_UUID + ", "
								   + UserDatabase.COLUMN_USERNAME + ", "
								   + UserDatabase.COLUMN_PASSWORD + ", "
								   + UserDatabase.COLUMN_EMAIL + ", "
								   + UserDatabase.COLUMN_FIRST_NAME + ", "
								   + UserDatabase.COLUMN_LAST_NAME + ", "
								   + UserDatabase.COLUMN_ADDRESS + ", "
								   + UserDatabase.COLUMN_GENDER + ", "
								   + UserDatabase.COLUMN_DOB + ", "
								   + UserDatabase.COLUMN_PHONE + ", "
								   + UserDatabase.COLUMN_IMAGE + ", "
								   + UserDatabase.COLUMN_ROLE + ", "
								   + UserDatabase.COLUMN_STATUS + ", "
								   + UserDatabase.COLUMN_CREATED_DATE
								   + " FROM " + UserDatabase.TABLE 
								   + " ORDER BY " + UserDatabase.COLUMN_USERNAME + " ASC";
			List<User> users = jdbcTemplate.query(sql, new UserRowMapper());
			if (users.isEmpty()) {				
				return null;
			}
			return users;
	}	

	@Override
	public boolean update(int id, User user) {
		String sql = "UPDATE " + UserDatabase.TABLE
					+ " SET " + UserDatabase.COLUMN_EMAIL+ " = ?, "
					+ UserDatabase.COLUMN_FIRST_NAME+ " = ?, "
					+ UserDatabase.COLUMN_LAST_NAME+ " = ?, "
					+ UserDatabase.COLUMN_ADDRESS+ " = ?, "
					+ UserDatabase.COLUMN_GENDER+ " = ?, "
					+ UserDatabase.COLUMN_DOB+ " = ?, "
					+ UserDatabase.COLUMN_PHONE+ " = ?, "
					+ UserDatabase.COLUMN_IMAGE+ " = ?, "
					+ UserDatabase.COLUMN_STATUS+ " = ? "
					+ "WHERE "+ UserDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                    StringFunction.trimSpace(user.getEmail()),
		                                    StringFunction.trimSpace(user.getFirstName()),
		                                    StringFunction.trimSpace(user.getLastName()),		                                    
		                                    StringFunction.trimSpace(user.getAddress()),		                                    
		                                    user.getGender(),
		                                    DateValidation.formatDate(user.getDob()),
		                                    user.getPhone(),
		                                    user.getImage(),
		                                    user.getStatus(), id);
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = "DELETE FROM " + UserDatabase.TABLE + " WHERE " + UserDatabase.COLUMN_ID + " = ? ";
		int rowDeleted = jdbcTemplate.update(sql, id);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}

	@Override
	public User detail(int id) {
		User user;
		try {
			String sql = "SELECT " + UserDatabase.COLUMN_ID + ", "
					   + "HEX(" + UserDatabase.COLUMN_UUID + ") as " + UserDatabase.COLUMN_UUID + ", "
					   + UserDatabase.COLUMN_USERNAME + ", "
					   + UserDatabase.COLUMN_PASSWORD + ", "
					   + UserDatabase.COLUMN_EMAIL + ", "
					   + UserDatabase.COLUMN_FIRST_NAME + ", "
					   + UserDatabase.COLUMN_LAST_NAME + ", "
					   + UserDatabase.COLUMN_ADDRESS + ", "
					   + UserDatabase.COLUMN_GENDER + ", "
					   + UserDatabase.COLUMN_DOB + ", "
					   + UserDatabase.COLUMN_PHONE + ", "
					   + UserDatabase.COLUMN_IMAGE + ", "
					   + UserDatabase.COLUMN_ROLE + ", "
					   + UserDatabase.COLUMN_STATUS + ", "
					   + UserDatabase.COLUMN_CREATED_DATE
					   + " FROM " + UserDatabase.TABLE 
					   + " WHERE " + UserDatabase.COLUMN_ID + " = ? ";
			user = jdbcTemplate.queryForObject(sql, new Object[] { id }, new UserRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return user;
	}

	@Override
	public boolean status(int id, int status) {
		try {
			String sql = "UPDATE " + UserDatabase.TABLE 
		               + " SET " + UserDatabase.COLUMN_STATUS + " = ? " 
		               + "WHERE " + UserDatabase.COLUMN_ID + " = ? ";
			int rowActive = jdbcTemplate.update(sql, status, id);
			return (rowActive > 0) ? true : false;  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected boolean deleteByUsername(String username) {
		String sql = "DELETE FROM " + UserDatabase.TABLE + " WHERE " + UserDatabase.COLUMN_USERNAME + " = ? ";
		int rowDeleted = jdbcTemplate.update(sql, username);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}
	
	protected boolean isUsernameExists(String username) {
		try {
			String sql = "SELECT " + UserDatabase.COLUMN_USERNAME 
								   + " FROM " + UserDatabase.TABLE
								   + " WHERE " + UserDatabase.COLUMN_USERNAME + " = ?";
			String dbUsername = jdbcTemplate.queryForObject(sql, new Object[] { username }, String.class);
			return username.equals(dbUsername) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected boolean isEmailExists(String email) {
		try {
			String sql = "SELECT " + UserDatabase.COLUMN_EMAIL 
								   + " FROM " + UserDatabase.TABLE
								   + " WHERE " + UserDatabase.COLUMN_EMAIL + " = ?";
			String dbEmail = jdbcTemplate.queryForObject(sql, new Object[] { email }, String.class);
			return email.equals(dbEmail) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected boolean changePassword(int id, User user) {
		try {
			String sql = " UPDATE " + UserDatabase.TABLE
					+ " SET "	+ UserDatabase.COLUMN_PASSWORD+ " = ? "
					+ "WHERE "	+ UserDatabase.COLUMN_ID+ " = ?";
			int rowActive = jdbcTemplate.update(sql, UserFunction.encoderPassword(user.getPassword()), id);
			return (rowActive > 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected User getUserByUsername(String username) {
		User user;
		try {
			String sql = " SELECT " + UserDatabase.COLUMN_ID + ", "
							+ "HEX(" + UserDatabase.COLUMN_UUID + ") as " + UserDatabase.COLUMN_UUID+ ", "
							+ UserDatabase.COLUMN_USERNAME + ", "
							+ UserDatabase.COLUMN_PASSWORD + ", "
							+ UserDatabase.COLUMN_EMAIL + ", "
							+ UserDatabase.COLUMN_FIRST_NAME + ", "
							+ UserDatabase.COLUMN_LAST_NAME + ", "
							+ UserDatabase.COLUMN_ADDRESS + ", "
							+ UserDatabase.COLUMN_GENDER + ", "
							+ UserDatabase.COLUMN_DOB + ", "
							+ UserDatabase.COLUMN_PHONE + ", "
							+ UserDatabase.COLUMN_IMAGE + ", "
							+ UserDatabase.COLUMN_ROLE + ", "
							+ UserDatabase.COLUMN_STATUS + ", "
							+ UserDatabase.COLUMN_CREATED_DATE
							+ " FROM " + UserDatabase.TABLE
							+ " WHERE "
							+  UserDatabase.COLUMN_USERNAME+ " = ? ";
			user = jdbcTemplate.queryForObject(sql, new Object[] { username }, new UserRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return user;
	}
	
	protected User getUserByUuid (String uuid) {
		User user;
		try {
			String sql = " SELECT " + UserDatabase.COLUMN_ID + ", "
									+ "HEX(" + UserDatabase.COLUMN_UUID + ") as " + UserDatabase.COLUMN_UUID+ ", "
									+ UserDatabase.COLUMN_USERNAME + ", "
									+ UserDatabase.COLUMN_PASSWORD + ", "
									+ UserDatabase.COLUMN_EMAIL + ", "
									+ UserDatabase.COLUMN_FIRST_NAME + ", "
									+ UserDatabase.COLUMN_LAST_NAME + ", "
									+ UserDatabase.COLUMN_ADDRESS + ", "
									+ UserDatabase.COLUMN_GENDER + ", "
									+ UserDatabase.COLUMN_DOB + ", "
									+ UserDatabase.COLUMN_PHONE + ", "
									+ UserDatabase.COLUMN_IMAGE + ", "
									+ UserDatabase.COLUMN_ROLE + ", "
									+ UserDatabase.COLUMN_STATUS + ", "
									+ UserDatabase.COLUMN_CREATED_DATE
									+ " FROM " + UserDatabase.TABLE
									+ " WHERE " + UserDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			user = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new UserRowMapper());
		} catch (Exception e) {
			return null;
		}
		return user;
	}
}
