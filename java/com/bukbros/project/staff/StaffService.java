package com.bukbros.project.staff;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.project.configuration.Role;
import com.bukbros.project.user.User;
import com.bukbros.project.user.UserService;

@Service
public class StaffService {
	@Autowired
	private StaffRepository repository;

	@Autowired
	private UserService userService;

	private User setUserIntoStaff(Staff staff) {
		User user = new User();
		user.setEmail(staff.getEmail());
		user.setFirstName(staff.getFirstName());
		user.setLastName(staff.getLastName());
		user.setAddress(staff.getAddress());
		user.setGender(staff.getGender());
		user.setDob(staff.getDob());
		user.setFile(staff.getFile());
		user.setPhone(staff.getPhone());
		user.setStatus(staff.getStatus());
		return user;
	}

	public boolean create(HttpServletRequest request, Staff staff) {
		User user = setUserIntoStaff(staff);
		user.setRole(Role.ROLE_STAFF);
		user.setUsername(staff.getUsername());
		user.setPassword(staff.getPassword());
		boolean createUser = userService.create(request, user);
		if (createUser) {
			User newUser = userService.getUserByUsername(user.getUsername());
			if (newUser != null) {
				staff.setUserId(newUser.getId());
				boolean createStaff = repository.create(staff);
				if (!createStaff) {
					userService.delete(request, newUser);
				}
				return createStaff;
			} else {
				userService.deleteByUsername(user.getUsername());
				return false;
			}
		}
		return createUser;
	}

	public boolean update(HttpServletRequest request, Staff originalStaff, Staff staff) {
		User user = setUserIntoStaff(staff);
		User originalUser = new User();
		originalUser.setId(originalStaff.getId());
		originalUser.setUsername(originalStaff.getUsername());
		originalUser.setImage(originalStaff.getImage());
		boolean updateUser = userService.update(request, originalUser, user);
		if (updateUser) {
			boolean updateStaff = repository.update(originalStaff.getUserId(), staff);
			return updateStaff;
		}
		return updateUser;
	}

	public List<Staff> getAllStaff() {
		return repository.read();
	}

	public Staff getStaffById(int id) {
		return repository.detail(id);
	}

	public Staff getStaffByUsername(String username) {
		return repository.getStaffByUsername(username);
	}
}
