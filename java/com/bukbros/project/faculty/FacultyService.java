package com.bukbros.project.faculty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacultyService {
	@Autowired
	private FacultyRepository repository;
	
	public List<Faculty> getAllFaculty() {
		return repository.read();
	}
	
	public boolean create(Faculty faculty) {
		return repository.create(faculty);
	}
	
	public boolean update(int id, Faculty faculty) {
		return repository.update(id, faculty);
	}
	
	public boolean status(int id, int status) {
		return repository.status(id, status);
	}
	
	public boolean delete(Faculty faculty) {
		return repository.delete(faculty.getId());
	}
	
	public Faculty getFacultyByUuid (String uuid) {
		return repository.getFacultyByUuid(uuid);
	}
	
	public boolean isNameExists(String name) {
		return repository.isNameExists(name);
	}
}
