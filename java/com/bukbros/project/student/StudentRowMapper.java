package com.bukbros.project.student;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;

public class StudentRowMapper implements RowMapper<Student>{
	public Student mapRow(ResultSet rs, int rowNo) throws SQLException {
		Student student = new Student();
		student.setId(rs.getInt(StudentDatabase.COLUMN_ID));
		student.setUuid(StringFunction.convertTextToUuid(rs.getString(StudentDatabase.COLUMN_UUID)));
		student.setUsername(rs.getString(StudentDatabase.COLUMN_USERNAME));
		student.setPassword(rs.getString(StudentDatabase.COLUMN_PASSWORD));
		student.setMajorId(rs.getInt(StudentDatabase.COLUMN_MAJOR_ID));
		student.setEmail(rs.getString(StudentDatabase.COLUMN_EMAIL));
		student.setFirstName(rs.getString(StudentDatabase.COLUMN_FIRST_NAME));
		student.setLastName(rs.getString(StudentDatabase.COLUMN_LAST_NAME));
		student.setAddress(rs.getString(StudentDatabase.COLUMN_ADDRESS));
		student.setDob(DateValidation.formatDateFromDatabase(rs.getString(StudentDatabase.COLUMN_DOB)));
		student.setGender(rs.getByte(StudentDatabase.COLUMN_GENDER));
		student.setPhone(rs.getString(StudentDatabase.COLUMN_PHONE));
		student.setStatus(rs.getByte(StudentDatabase.COLUMN_STATUS));
		student.setCreatedDate(rs.getDate(StudentDatabase.COLUMN_CREATED_DATE));
		return student;
	}
}
