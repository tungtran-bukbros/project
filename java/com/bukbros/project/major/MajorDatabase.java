package com.bukbros.project.major;

public class MajorDatabase {
	public static final String TABLE = "cr_majors";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_CODE = "code";
	public static final String COLUMN_STATUS = "status";
}
