package com.bukbros.project.time;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Url;
import com.bukbros.project.detail.Detail;
import com.bukbros.project.detail.DetailService;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;

@Controller
@RequestMapping(value = TimeConfig.MODULE_ADMIN_URL)
public class TimeController {
	@Autowired
	private TimeService service;
	@Autowired
	private DetailService detailService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return TimeConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return TimeLanguage.label();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getTime(Model model, Time time) {
		model.addAttribute("listMode", true);
		List<Time> times = service.getAllTime();
		List<Detail> details = detailService.getAllDetail();
		List<Time> groups = service.groupByDetailId();
		if (times == null || details == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("details", details);
			model.addAttribute("times", times);
			model.addAttribute("groups", groups);
		}
		return TimeConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String getCreate(Model model) {
		List<Detail> details = detailService.getAllDetail();
		model.addAttribute("createMode", true);
		model.addAttribute("details", details);
		return TimeConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CREATE)
	public String postCreate(	@ModelAttribute("validTime") @Valid Time time,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {
		List<Detail> details = detailService.getAllDetail();
		model.addAttribute("createMode", true);
		model.addAttribute("details", details);
		model.addAttribute("time", time);
		if (time.getBlocks() == null) {
			results.addError(new FieldError("validTime", "blocks", TimeLanguage.VALIDATION_NOT_EMPTY));
		}
		if (results.hasFieldErrors("blocks")) {
			return TimeConfig.MODULE_VIEW_ADMIN;
		}
		boolean hasError = false;
		for (String block : time.getBlocks()) {
			if (service.isTimeExists(time.getDetailId(), time.getWeekDay(), block) != null) {
				hasError = true;
			}
		}
		if (hasError) {
			model.addAttribute("errorMessage", TimeLanguage.VALIDATION_TIME_EXISTS);
			return TimeConfig.MODULE_VIEW_ADMIN;
		}		
		boolean createItem = service.create(time);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_TIME;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid,
							@RequestParam(name = "weekDay", defaultValue = "0") String weekDay,
							Model model,
							RedirectAttributes redirectAttributes) {
		Detail detail = detailService.getDetailByUuid(uuid);
		if (detail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		List<String> blocks = service.getBlocksByDetailIdWeekDay(detail.getId(), weekDay);
		boolean validWeekDay = service.validWeekDay(weekDay, TimeConfig.EXT_WEEKDAY);
		if (blocks == null || !validWeekDay) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		boolean validWeekDaysByDetailId = service.validWeekDaysByDetailId(weekDay, detail.getId());
		if (!validWeekDaysByDetailId) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		Time time = new Time();
		time.setBlocks(blocks);
		model.addAttribute("details", detailService.getAllDetail());
		model.addAttribute("detail", detail);
		model.addAttribute("time", time);
		model.addAttribute("updateMode", true);
		return TimeConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(	@PathVariable("uuid") String uuid,
								@ModelAttribute("validTime") @Valid Time time,
								@RequestParam(name = "weekDay", defaultValue = "0") String weekDay,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {

		Detail originalDetail = detailService.getDetailByUuid(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		
		model.addAttribute("updateMode", true);
		model.addAttribute("time", time);
		model.addAttribute("details", detailService.getAllDetail());
		model.addAttribute("detail", originalDetail);
		if (time.getBlocks() == null) {
			results.addError(new FieldError("validTime", "blocks", TimeLanguage.VALIDATION_NOT_EMPTY));
		}
		if (results.hasFieldErrors("blocks")) {
			return TimeConfig.MODULE_VIEW_ADMIN;
		}
		boolean validWeekDay = service.validWeekDay(weekDay, TimeConfig.EXT_WEEKDAY);
		if (!validWeekDay) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		boolean validWeekDaysByDetailId = service.validWeekDaysByDetailId(weekDay, originalDetail.getId());
		if (!validWeekDaysByDetailId) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		if (service.update(originalDetail.getId(), weekDay, time)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_TIME;
	}
	
	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String activeTime (  @PathVariable("uuid") String uuid,
	                            @RequestParam(name = "weekDay", defaultValue = "0") String weekDay,
	                            RedirectAttributes redirectAttributes) {
		Detail originalDetail = detailService.getDetailByUuid(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		boolean active = service.status(originalDetail.getId(), weekDay, 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_TIME;
	}
	
	@PostMapping( "/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveTime (  @PathVariable("uuid") String uuid,
	                              @RequestParam(name = "weekDay", defaultValue = "0") String weekDay,
	                              RedirectAttributes redirectAttributes) {
		Detail originalDetail = detailService.getDetailByUuid(uuid);
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME;
		}
		boolean deactive = service.status(originalDetail.getId(), weekDay, 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_TIME;
	}
}
