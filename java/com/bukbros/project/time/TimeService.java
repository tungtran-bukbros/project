package com.bukbros.project.time;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimeService {
	@Autowired
	private TimeRepository repository;

	public boolean create(Time time) {
		boolean createTime = true;
		for (String block : time.getBlocks()) {
			time.setBlock(block);
			createTime = repository.create(time);
			if (!createTime) {
				repository.deleteBlock(time.getDetailId(), time.getWeekDay());
				return createTime;
			}
		}
		return createTime;
	}

	public List<Time> getAllTime() {
		return repository.read();
	}

	public boolean update(int detailId, String weekDay, Time time) {
		time.setDetailId(detailId);
		boolean deleteTime = repository.deleteBlock(detailId, weekDay);
		if (deleteTime) {
			boolean createTime = true;
			for (String block : time.getBlocks()) {
				time.setBlock(block);
				createTime = repository.create(time);
				if (!createTime) {
					return false;
				}
			}
			return createTime;
		}
		return false;
	}
	
	public boolean status(int detailId, String weekDay, int status) {
		return repository.status(detailId, weekDay, status);
	}

	public Time isTimeExists(int detailId, String weekDay, String block) {
		return repository.isTimeExists(detailId, weekDay, block);
	}

	public List<Time> groupByDetailId() {
		return repository.groupByDetailId();
	}

	public boolean validWeekDay(String weekDay, String[] exts) {
		for (String ext : exts) {
			if (ext.equals(weekDay)) {
				return true;
			}
		}
		return false;
	}

	public boolean validWeekDaysByDetailId(String ext, int detailId) {
		List<Integer> weekDays = repository.getWeekDaysByDetailId(detailId);
		for (Integer weekDay : weekDays) {
			if (ext.equals("" + weekDay)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getBlocksByDetailIdWeekDay(int detailId, String weekDay) {
		return repository.getBlocksByDetailIdWeekDay(detailId, weekDay);
	}
}
