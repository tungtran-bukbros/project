package com.bukbros.project.faculty;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.StringFunction;

public class FacultyRowMapper implements RowMapper<Faculty>{
	public Faculty mapRow(ResultSet rs, int rowNo) throws SQLException {
		Faculty faculty = new Faculty();
		faculty.setId(rs.getInt(FacultyDatabase.COLUMN_ID));
		faculty.setUuid(StringFunction.convertTextToUuid(rs.getString(FacultyDatabase.COLUMN_UUID)));
		faculty.setName(rs.getString(FacultyDatabase.COLUMN_NAME));
		faculty.setStatus(rs.getByte(FacultyDatabase.COLUMN_STATUS));
		return faculty;
	}

}
