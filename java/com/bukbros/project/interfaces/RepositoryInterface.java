package com.bukbros.project.interfaces;

import java.util.List;

public interface RepositoryInterface<Obj extends Object> {
	public boolean create(Obj object);
	public List<? extends Object> read();
	public boolean update(int id, Obj object);
	public boolean delete(int id);
	public Object detail(int id);
	public boolean status(int id, int status);
}
