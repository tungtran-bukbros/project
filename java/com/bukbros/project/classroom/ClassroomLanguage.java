package com.bukbros.project.classroom;

import java.util.HashMap;

public class ClassroomLanguage {
	public static final String VALIDATION_SPECIAL_CHARACTERS = "Không được sử dụng ký tự đặc biệt";	
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	
	public static final String LABEL_SLUG = "Slug";
	public static final String LABEL_NAME = "Tên lớp";
	
	public static final String LABEL_CREATE = "Thêm mới lớp học";
	public static final String LABEL_UPDATE = "Cập nhật lớp học";
	public static final String LABEL_LIST = "Danh sách lớp học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_SLUG", LABEL_SLUG);
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		return labels;
	}
}
