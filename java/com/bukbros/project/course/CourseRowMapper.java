package com.bukbros.project.course;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;

public class CourseRowMapper implements RowMapper<Course>{
	public Course mapRow(ResultSet rs, int rowNo) throws SQLException {
		Course course = new Course();
		course.setId(rs.getInt(CourseDatabase.COLUMN_ID));
		course.setUuid(StringFunction.convertTextToUuid(rs.getString(CourseDatabase.COLUMN_UUID)));
		course.setCode(rs.getString(CourseDatabase.COLUMN_CODE));
		course.setStartTime(DateValidation.formatDateFromDatabase(rs.getString(CourseDatabase.COLUMN_START_TIME)));
		course.setEndTime(DateValidation.formatDateFromDatabase(rs.getString(CourseDatabase.COLUMN_END_TIME)));		
		return course;
	}

}
