package com.bukbros.project.user;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bukbros.project.configuration.GlobalConfiguration;
import com.bukbros.project.function.DateValidation;
import com.bukbros.project.upload.UploadService;

@Service
public class UserService {
	@Autowired
	private UserRepository repository;

	@Autowired
	private UploadService uploadService;

	public boolean create(HttpServletRequest request, User user) {
		MultipartFile file = user.getFile();
		if (file.getSize() > 0) {
			String fileName = uploadService.generateFileName(file, user.getUsername());
			boolean saveFile = uploadService.saveFile(file, fileName, GlobalConfiguration.USER_AVATAR_FOLDER, request);
			if (saveFile) {
				user.setImage(GlobalConfiguration.USER_AVATAR_LINK + fileName);
			} else {
				return false;
			}
		} else {
			return false;
		}
		boolean createUser = repository.create(user);
		if (!createUser) {
			uploadService.deleteFile(user.getImage().replace("/resources", "upload"), request);
		}
		return createUser;
	}

	public List<User> getAllUser() {
		return repository.read();
	}

	public boolean update(HttpServletRequest request, User originalUser, User user) {
		MultipartFile file = user.getFile();
		boolean saveFile = false;
		if (file.getSize() > 0) {
			String fileName = uploadService.generateFileName(file, originalUser.getUsername());
			saveFile = uploadService.saveFile(file, fileName, GlobalConfiguration.USER_AVATAR_FOLDER, request);
			if (saveFile) {
				user.setImage(GlobalConfiguration.USER_AVATAR_LINK + fileName);
			} else {
				return false;
			}
		} else {
			user.setImage(originalUser.getImage());
		}
		boolean updateUser = repository.update(originalUser.getId(), user);
		if (updateUser) {
			uploadService.deleteFile(originalUser.getImage().replace("/resources", "upload"), request);
		} else {
			if (saveFile) {
				uploadService.deleteFile(user.getImage().replace("/resources", "upload"), request);
			} 
		}
		return updateUser;
	}

	public boolean isUsernameExists(String username) {
		return repository.isUsernameExists(username);
	}

	public boolean isEmailExists(String email) {
		return repository.isEmailExists(email);
	}

	public boolean confirmPassword(String password, String confirmPassword) {
		if (password.equals(confirmPassword)) {
			return true;
		}
		return false;
	}

	public boolean maxDate(String dob) {
		Date date = DateValidation.convertStringToDate(dob);
		Date today = new Date();
		if (date.after(today)) {
			return true;
		}
		return false;
	}

	public User getUserById(int id) {
		return repository.detail(id);
	}

	public boolean status(int id, int status) {
		return repository.status(id, status);
	}

	public boolean delete(HttpServletRequest request, User user) {
		boolean deleteUser =  repository.delete(user.getId());
		if (deleteUser) {
			uploadService.deleteFile(user.getImage().replace("/resources", "upload"), request);
		}		
		return deleteUser;
	}
		
	public boolean deleteByUsername(String username) {
		return repository.deleteByUsername(username);
	}

	public boolean changePassword(int id, User user) {
		return repository.changePassword(id, user);
	}

	public User getUserByUsername(String username) {
		return repository.getUserByUsername(username);
	}

	public boolean checkPassword(String oldPassword, String encryptedPassword) {
		return UserFunction.checkPassword(oldPassword, encryptedPassword);
	}

	public User getUserByUuid(String uuid) {
		return repository.getUserByUuid(uuid);
	}
}
