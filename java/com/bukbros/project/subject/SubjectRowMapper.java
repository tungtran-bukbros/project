package com.bukbros.project.subject;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.StringFunction;

public class SubjectRowMapper implements RowMapper<Subject>{
	public Subject mapRow(ResultSet rs, int rowNo) throws SQLException {
		Subject subject = new Subject();
		subject.setId(rs.getInt(SubjectDatabase.COLUMN_ID));
		subject.setUuid(StringFunction.convertTextToUuid(rs.getString(SubjectDatabase.COLUMN_UUID)));
		subject.setTitle(rs.getString(SubjectDatabase.COLUMN_TITLE));
		subject.setCode(rs.getString(SubjectDatabase.COLUMN_CODE));
		subject.setCreadit(rs.getString(SubjectDatabase.COLUMN_CREADIT));
		subject.setStatus(rs.getByte(SubjectDatabase.COLUMN_STATUS));
		return subject;
	}
}
