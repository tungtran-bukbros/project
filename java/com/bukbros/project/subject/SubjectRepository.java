package com.bukbros.project.subject;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.StringFunction;
import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class SubjectRepository implements RepositoryInterface<Subject>{
	@Autowired
	private JdbcTemplate jdbcTemplate;	
	
	
	@Override
	public boolean create(Subject subject) {
		String sql = "INSERT INTO " + SubjectDatabase.TABLE
					+ " ("
					+ SubjectDatabase.COLUMN_UUID+ ", "
					+ SubjectDatabase.COLUMN_TITLE+ ", "
					+ SubjectDatabase.COLUMN_CODE+ ", "
					+ SubjectDatabase.COLUMN_CREADIT+ ", "
					+ SubjectDatabase.COLUMN_STATUS
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     subject.getTitle(),
		                                     subject.getCode(),
		                                     subject.getCreadit(),
		                                     subject.getStatus());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Subject> read() {
		String sql = "SELECT " + SubjectDatabase.COLUMN_ID + ", "
				   + "HEX(" + SubjectDatabase.COLUMN_UUID + ") as " + SubjectDatabase.COLUMN_UUID + ", "
				   + SubjectDatabase.COLUMN_TITLE + ", "
				   + SubjectDatabase.COLUMN_CODE + ", "
				   + SubjectDatabase.COLUMN_CREADIT + ", "
				   + SubjectDatabase.COLUMN_STATUS
				   + " FROM " + SubjectDatabase.TABLE 
				   + " ORDER BY " + SubjectDatabase.COLUMN_ID + " ASC";
		List<Subject> subjects = jdbcTemplate.query(sql, new SubjectRowMapper());
		if (subjects.isEmpty()) {
			return null;
		}
		return subjects;
	}

	@Override
	public boolean update(int id, Subject subject) {
		String sql = "UPDATE " + SubjectDatabase.TABLE
					+ " SET " + SubjectDatabase.COLUMN_TITLE+ " = ?, "
					+ SubjectDatabase.COLUMN_CODE+ " = ?, "
					+ SubjectDatabase.COLUMN_CREADIT+ " = ?, "
					+ SubjectDatabase.COLUMN_STATUS+ " = ? "
					+ " WHERE " + SubjectDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                     StringFunction.trimSpace(subject.getTitle()),
		                                     StringFunction.trimSpace(subject.getCode()),
		                                     subject.getCreadit(),
		                                     subject.getStatus(), id);
					
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = "DELETE FROM " + SubjectDatabase.TABLE + " WHERE " + SubjectDatabase.COLUMN_ID + " = ? ";
		int rowDeleted = jdbcTemplate.update(sql, id);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean status(int id, int status) {
		try {
			String sql = "UPDATE " + SubjectDatabase.TABLE 
		               + " SET " + SubjectDatabase.COLUMN_STATUS + " = ? " 
		               + "WHERE " + SubjectDatabase.COLUMN_ID + " = ? ";
			int rowActive = jdbcTemplate.update(sql, status, id);
			return (rowActive > 0) ? true : false;  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected boolean isTitleExists(String title) {
		try {
			String sql = "SELECT " + SubjectDatabase.COLUMN_TITLE 
								   + " FROM " + SubjectDatabase.TABLE
								   + " WHERE " + SubjectDatabase.COLUMN_TITLE + " = ?";
			String dbTitle = jdbcTemplate.queryForObject(sql, new Object[] { title }, String.class);
			return title.equals(dbTitle) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected boolean isCodeExists(String code) {
		try {
			String sql = "SELECT " + SubjectDatabase.COLUMN_CODE 
								   + " FROM " + SubjectDatabase.TABLE
								   + " WHERE " + SubjectDatabase.COLUMN_CODE + " = ?";
			String dbCode = jdbcTemplate.queryForObject(sql, new Object[] { code }, String.class);
			return code.equals(dbCode) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected Subject getSubjectByUuid (String uuid) {
		Subject subject;
		try {
			String sql = " SELECT " + SubjectDatabase.COLUMN_ID + ", "
								+ "HEX(" + SubjectDatabase.COLUMN_UUID + ") as " + SubjectDatabase.COLUMN_UUID+ ", "
								+ SubjectDatabase.COLUMN_TITLE + ", "
							    + SubjectDatabase.COLUMN_CODE + ", "
							    + SubjectDatabase.COLUMN_CREADIT + ", "
							    + SubjectDatabase.COLUMN_STATUS					    
								+ " FROM " + SubjectDatabase.TABLE
								+ " WHERE " + SubjectDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			subject = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new SubjectRowMapper());
		} catch (Exception e) {
			return null;
		}
		return subject;
	}

}
