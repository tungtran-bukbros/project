<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${urlGlobals.URL_STAFF}' />' class="btn btn-info"><i class="fas fa-bars"></i> Danh sách</a>
		</div>
	</c:if>
	<c:if test="${updateMode || updateCurrentMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${urlGlobals.URL_STAFF.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
				${labelLang.LABEL_BUTTON_CREATE}
			</a> <a href='<c:url value='${urlGlobals.URL_STAFF}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${urlGlobals.URL_STAFF.concat(urlGlobals.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${urlGlobals.URL_STAFF.concat('/').concat(staff.uuid).concat(urlGlobals.URL_UPDATE)}"></c:url>
</c:if>
<c:if test="${updateCurrentMode}">
	<c:url var="urlForm" value="${urlGlobals.URL_STAFF.concat(urlGlobals.URL_UPDATE)}" />
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validStaff" enctype="multipart/form-data">
	<c:if test="${createMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Tên đăng nhập</label>
			<div class="col-sm-7">
				<input type="text" name="username" class="form-control" value="${staff.username}" placeholder="Ví dụ: baoanh" path="username">
				<form:errors path="username" cssClass="error" />
			</div>
		</div>
	</c:if>
	<c:if test="${updateCurrentMode}">
		<div class="form-group row">
			<label for="username" class="col-sm-3 col-form-label text-right">Tên truy cập</label>
			<div class="col-sm-7 col-form-label">${currentUser.username}</div>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="form-group row">
			<label for="username" class="col-sm-3 col-form-label text-right">Tên truy cập</label>
			<div class="col-sm-7 col-form-label">${staff.username}</div>
		</div>
	</c:if>
	<c:if test="${createMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu</label>
			<div class="col-sm-7">
				<input type="password" name="password" class="form-control" placeholder="Mật khẩu" path="password">
				<form:errors path="password" cssClass="error" />
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Nhập lại mật khẩu</label>
			<div class="col-sm-7">
				<input type="password" name="confirmPassword" class="form-control" placeholder="Nhập lại mật khẩu" path="confirmPassword">
				<form:errors path="confirmPassword" cssClass="error" />
			</div>
		</div>
	</c:if>
	<c:if test="${updateMode || updateCurrentMode}">
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Mật khẩu</label>
			<div class="col-sm-7">
				<a href='<c:url value="${urlGlobals.URL_STAFF.concat('/').concat(staff.uuid).concat(urlGlobals.URL_CHANGE_PASSWORD)}"/>' class="btn btn-primary">
					Đổi mật khẩu</a>
			</div>
		</div>
	</c:if>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Email</label>
		<div class="col-sm-7">
			<input type="text" name="email" class="form-control" value="${staff.email}" placeholder="Ví dụ: contact@email.com" path="email">
			<form:errors path="email" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Họ</label>
		<div class="col-sm-7">
			<input type="text" name="firstName" class="form-control" value="${staff.firstName}" placeholder="Ví dụ: Trần" path="firstName">
			<form:errors path="firstName" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Tên</label>
		<div class="col-sm-7">
			<input type="text" name="lastName" class="form-control" value="${staff.lastName}" placeholder="Ví dụ: Bảo Anh" path="lastName">
			<form:errors path="lastName" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Khoa</label>
		<div class="col-sm-7">
			<select class="form-control" name="facultyId" path="facultyId">
				<c:forEach var="faculty" items="${faculties}">
					<option value="${faculty.id}" ${faculty.id == staff.facultyId ? 'selected' : ''}>${faculty.name}</option>
				</c:forEach>
			</select>
			<form:errors path="facultyId" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Địa chỉ</label>
		<div class="col-sm-7">
			<input type="text" name="address" class="form-control" value="${staff.address}"
				placeholder="Ví dụ: 38 Nguyễn Khánh Toàn, Quan Hoa, Cầu Giấy, Hà Nội" path="address">
			<form:errors path="address" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Giới tính</label>
		<div class="form-group col-sm-3">
			<input type="radio" name="gender" value="1" checked> <i class="fas fa-male"></i> Nam
		</div>
		<div class="col-sm-3">
			<input type="radio" name="gender" value="0" ${staff.gender == 0 ? 'checked="checked"' : ''}> <i class="fas fa-female"></i> Nữ
		</div>
		<div class="col-sm-3">
			<input type="radio" name="gender" value="2" ${staff.status == 2 ? 'checked="checked"' : ''}> Khác
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="dob" value="${staff.dob}" placeholder="Định dạng : ngày/tháng/năm" path="dob">
			<form:errors path="dob" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Số điện thoại ${staff.certificate}</label>
		<div class="col-sm-7">
			<input type="text" class="form-control datepicker-top" name="phone" value="${staff.phone}" placeholder="Ví dụ: 0912345678" path="phone">
			<form:errors path="phone" cssClass="error" />
		</div>
	</div>
	<!-- Avatar -->
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Avatar</label>
		<c:choose>
			<c:when test="${createMode}">
				<div class="col-sm-7">
					<input type="file" class="form-control" name="file" path="file"> <small id="avatarFileHelp" class="form-text text-muted">Chỉ chấp
						nhận file <b class="text-uppercase">jpg</b> hoặc <b class="text-uppercase">png</b>. Dung lượng file không vượt quá <b>512 kB</b> (0.5 MB)
					</small>
					<form:errors path="file" cssClass="error" />
				</div>
			</c:when>
			<c:when test="${updateMode}">
				<div class="col-sm-5">
					<input type="file" class="form-control" name="file" path="file"> <small id="avatarFileHelp" class="form-text text-muted">Chỉ chấp
						nhận file <b class="text-uppercase">jpg</b> hoặc <b class="text-uppercase">png</b>. Dung lượng file không vượt quá <b>512 kB</b> (0.5 MB)
					</small>
					<form:errors path="file" cssClass="error" />
				</div>
				<div class="col-sm-2">
					<img src="<c:url value="${originalStaff.image}" />" border="0" width="100%">
				</div>
			</c:when>
			<c:when test="${updateCurrentMode}">
				<div class="col-sm-2">
					<img src="<c:url value="${currentUser.image}" />" border="0" width="100%">
				</div>
			</c:when>
		</c:choose>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Chứng chỉ đạt được</label>
		<div class="col-sm-7">
			<textarea class="form-control" name="certificate" rows="3" cols="80">${staff.certificate}</textarea>
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Trạng thái</label>
		<div class="col-sm-7 col-form-label">
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="active" value="1" checked="checked"> <label
					class="custom-control-label" for="active"> Kích hoạt</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input class="custom-control-input" type="radio" name="status" id="deactive" value="0" ${staff.status == 0 ? 'checked="checked"' : ''}> <label
					class="custom-control-label" for="deactive"> Tạm dừng</label>
			</div>
		</div>
	</div>
	<hr />
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-9 offset-sm-3">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode || updateCurrentMode}">
			<div class="offset-sm-3 col-sm-7">
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
