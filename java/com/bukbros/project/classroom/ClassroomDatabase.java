package com.bukbros.project.classroom;

public class ClassroomDatabase {
	public static final String TABLE = "cr_class";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_UUID = "uuid";
	public static final String COLUMN_SLUG = "slug";
	public static final String COLUMN_NAME = "name";
	
	public static final String TABLE_CLASS_SUBJECT = "cr_class_subject";
	public static final String COLUMN_CLASS_ID = "class_id";
	public static final String COLUMN_SUBJECT_ID = "subject_id";
}
