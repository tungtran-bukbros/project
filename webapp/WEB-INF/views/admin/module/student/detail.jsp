<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row mb-3">
	<div class="col-sm-3">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Mã sinh viên</li>
			<li class="list-group-item">Họ và tên</li>
			<li class="list-group-item">Ngành học</li>
			<li class="list-group-item">Ngày sinh</li>
			<li class="list-group-item">Lớp</li>
			<li class="list-group-item">Giáo viên chủ nhiệm</li>
			<li class="list-group-item">Điện thoại</li>
			<li class="list-group-item">Email SV</li>
			<li class="list-group-item">Địa chỉ</li>
		</ul>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">${student.username}</li>
			<li class="list-group-item">${student.firstName.concat(" ").concat(student.lastName)}</li>
			<c:forEach var="major" items="${majors}">
				<c:if test="${student.majorId == major.id}">
					<li class="list-group-item">${major.name}</li>
				</c:if>
			</c:forEach>
			<li class="list-group-item">${student.dob}</li>
			<li class="list-group-item">...</li>
			<li class="list-group-item">...</li>
			<li class="list-group-item">${student.phone}</li>
			<li class="list-group-item">${student.email}</li>
			<li class="list-group-item">${student.address}</li>
		</ul>
	</div>
</div>
