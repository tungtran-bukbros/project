package com.bukbros.project.configuration;

import java.util.HashMap;

public class Role {
	private final static String ROLE_PREFIX = "ROLE_";
	public final static String ROLE_ADMIN = ROLE_PREFIX + "ADMIN";
	public final static String ROLE_STAFF = ROLE_PREFIX + "STAFF";
	public final static String ROLE_USER = ROLE_PREFIX + "USER";
	
	public static HashMap<String, String> roles(){
		HashMap<String, String> roles = new HashMap<>();
		roles.put("ROLE_ADMIN", ROLE_ADMIN);
		roles.put("ROLE_STAFF", ROLE_STAFF);
		roles.put("ROLE_USER", ROLE_USER);
		return roles;
	}
	
}
