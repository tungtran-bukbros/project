package com.bukbros.project.student;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bukbros.project.function.DateValidation;

@Service
public class StudentService {
	@Autowired
	private StudentRepository repository;
	
	public List<Student> getAllStudent() {
		return repository.getAllStudent();
	}
	
	public boolean create(Student student) {
		return repository.create(student);
	}
	
	public boolean update(int id, Student student) {
		return	repository.update(id, student);
	}
	
	public Student getStudentById(int id) {
		return repository.detail(id);
	}
	
	public boolean isUsernameExists(String username) {
		return repository.isUsernameExists(username);
	}

	public boolean isEmailExists(String email) {
		return repository.isEmailExists(email);
	}
	
	public boolean confirmPassword (Student student) {
		if (student.getPassword().equals(student.getConfirmPassword())) {
			return true;
		}
		return false;
	}
	
	public boolean maxDate(String dob) {
		Date date = DateValidation.convertStringToDate(dob);
		Date today = new Date();
		if (date.after(today)) {
			return true;
		}
		return false;
	}
	
	public boolean changePassword(int id, Student student) {
		return repository.changePassword(id, student);
	}
	
	public Student getStudentByUsername(String username) {
		return repository.getStudentByUsername(username);
	}
	
	public boolean checkPassword(String oldPassword, String encryptedPassword) {
		return StudentFunction.checkPassword(oldPassword, encryptedPassword);
	}
	
	public Student getStudentByUuid (String uuid) {
		return repository.getStudentByUuid(uuid);
	}
	
	public boolean status(int id, int status) {
		return repository.status(id, status);
	}
}
