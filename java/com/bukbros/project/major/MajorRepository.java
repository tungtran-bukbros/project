package com.bukbros.project.major;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.interfaces.RepositoryInterface;
@Repository
public class MajorRepository implements RepositoryInterface<Major>{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean create(Major major) {
		String sql = "INSERT INTO " + MajorDatabase.TABLE
					+ " ("
					+ MajorDatabase.COLUMN_UUID+ ", "
					+ MajorDatabase.COLUMN_NAME+ ", "
					+ MajorDatabase.COLUMN_CODE+ ", "
					+ MajorDatabase.COLUMN_STATUS
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     major.getName(),
		                                     major.getCode(),
		                                     major.getStatus());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Major> read() {
		String sql = "SELECT " + MajorDatabase.COLUMN_ID + ", "
				   + "HEX(" + MajorDatabase.COLUMN_UUID + ") as " + MajorDatabase.COLUMN_UUID + ", "
				   + MajorDatabase.COLUMN_NAME + ", "
				   + MajorDatabase.COLUMN_CODE + ", "
				   + MajorDatabase.COLUMN_STATUS
				   + " FROM " + MajorDatabase.TABLE 
				   + " ORDER BY " + MajorDatabase.COLUMN_ID + " ASC";
		List<Major> majors = jdbcTemplate.query(sql, new MajorRowMapper());
		if (majors.isEmpty()) {
			return null;
		}
		return majors;
	}

	@Override
	public boolean update(int id, Major major) {
		String sql = "UPDATE " + MajorDatabase.TABLE
					+ " SET " + MajorDatabase.COLUMN_CODE+ " = ?, "
					+ MajorDatabase.COLUMN_NAME+ " = ?, "
					+ MajorDatabase.COLUMN_STATUS+ " = ? "
					+ " WHERE " + MajorDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                     major.getCode(),
		                                     major.getName(),
		                                     major.getStatus(), id);
					
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = "DELETE FROM " + MajorDatabase.TABLE + " WHERE " + MajorDatabase.COLUMN_ID + " = ? ";
		int rowDeleted = jdbcTemplate.update(sql, id);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean status(int id, int status) {
		try {
			String sql = "UPDATE " + MajorDatabase.TABLE 
		               + " SET " + MajorDatabase.COLUMN_STATUS + " = ? " 
		               + "WHERE " + MajorDatabase.COLUMN_ID + " = ? ";
			int rowActive = jdbcTemplate.update(sql, status, id);
			return (rowActive > 0) ? true : false;  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected Major getMajorByUuid (String uuid) {
		Major major;
		try {
			String sql = " SELECT " + MajorDatabase.COLUMN_ID + ", "
								+ "HEX(" + MajorDatabase.COLUMN_UUID + ") as " + MajorDatabase.COLUMN_UUID+ ", "
							    + MajorDatabase.COLUMN_CODE + ", "
								+ MajorDatabase.COLUMN_NAME + ", "
							    + MajorDatabase.COLUMN_STATUS					    
								+ " FROM " + MajorDatabase.TABLE
								+ " WHERE " + MajorDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			major = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new MajorRowMapper());
		} catch (Exception e) {
			return null;
		}
		return major;
	}
	
	protected boolean isCodeExists(String code) {
		try {
			String sql = "SELECT " + MajorDatabase.COLUMN_CODE 
								   + " FROM " + MajorDatabase.TABLE
								   + " WHERE " + MajorDatabase.COLUMN_CODE + " = ?";
			String dbCode = jdbcTemplate.queryForObject(sql, new Object[] { code }, String.class);
			return code.equals(dbCode) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected boolean isNameExists(String name) {
		try {
			String sql = "SELECT " + MajorDatabase.COLUMN_NAME 
								   + " FROM " + MajorDatabase.TABLE
								   + " WHERE " + MajorDatabase.COLUMN_NAME + " = ?";
			String dbCode = jdbcTemplate.queryForObject(sql, new Object[] { name }, String.class);
			return name.equals(dbCode) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
