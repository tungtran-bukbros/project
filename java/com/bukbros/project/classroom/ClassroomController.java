package com.bukbros.project.classroom;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Url;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;

@Controller
@RequestMapping(value = ClassroomConfig.MODULE_ADMIN_URL)
public class ClassroomController {
	@Autowired
	private ClassroomService service;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return ClassroomConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return ClassroomLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getClassroom(Model model) {
		model.addAttribute("listMode", true);
		List<Classroom> classrooms = service.getAllClassroom();
		if (classrooms == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("classrooms", classrooms);
		}
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (Model model) {
		model.addAttribute("createMode", true);
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validClassroom") @Valid Classroom classroom,
							  BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("classroom", classroom);
		if (results.hasFieldErrors("name")) {
			return ClassroomConfig.MODULE_VIEW_ADMIN;
		}
		boolean createItem = service.create(classroom);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_CLASSROOM;
	}
	
	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate (@PathVariable("uuid") String uuid,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		Classroom originalClassroom = service.getClassroomByUuid(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_CLASSROOM;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("classroom", originalClassroom);
		return ClassroomConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
	                          @ModelAttribute("validSubject") @Valid Classroom classroom,
	                          BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		Classroom originalClassroom = service.getClassroomByUuid(uuid);
		if (originalClassroom == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_CLASSROOM;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("classroom", classroom);
		if (results.hasFieldErrors("name")) {
			return ClassroomConfig.MODULE_VIEW_ADMIN;
		}
		if (service.update(originalClassroom.getId(), classroom)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_CLASSROOM;
	}
	
	@PostMapping("/{uuid}" + Url.URL_DELETE )
	public String deleteSubject(@PathVariable("uuid") String uuid,
	                            Model model,
	                            RedirectAttributes redirectAttributes) {
		Classroom classroomByUuid = service.getClassroomByUuid(uuid);
		if (classroomByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_CLASSROOM;
		}
		boolean delete = service.delete(classroomByUuid);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_FAILURE);
		}
		return "redirect:" + Url.URL_CLASSROOM;
	}	
}
