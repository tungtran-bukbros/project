package com.bukbros.project.user;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class User {
	private int id;
	private String uuid;
	
	@Size(min = 4, max = 30, message = UserLanguage.VALIDATION_SIZE_MIN_4_MAX_30)
	@Pattern(regexp = "[a-zA-Z0-9]+", message = UserLanguage.VALIDATION_REGEX_a_A_0)
	private String username;
	
	@Size(min = 6, max = 30, message = UserLanguage.VALIDATION_SIZE_MIN_6_MAX_30)
	@Pattern(regexp = "[^ ]+", message = UserLanguage.VALIDATION_REGEX_NOT_SPACE)
	private String password;
	
	private String confirmPassword;
	
	private String oldPassword;
	
	@NotBlank(message = UserLanguage.VALIDATION_NOT_EMPTY)
	@Email(message = UserLanguage.VALIDATION_EMAIL)
	private String email;
	
	@NotBlank(message = UserLanguage.VALIDATION_NOT_EMPTY)
	@Size(max = 50, message = UserLanguage.VALIDATION_MAX_50)
	@Pattern(regexp = "[^0-9_+-.,!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = UserLanguage.VALIDATION_FIRSTNAME)
	private String firstName;
	
	@NotBlank(message = UserLanguage.VALIDATION_NOT_EMPTY)
	@Size(max = 50, message = UserLanguage.VALIDATION_MAX_50)
	@Pattern(regexp = "[^0-9_+-.,!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = UserLanguage.VALIDATION_LASTNAME)
	private String lastName;
	
	@NotBlank(message = UserLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[^!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = UserLanguage.VALIDATION_SPECIAL_CHARACTERS)
	private String address;
	
	private byte gender;
	
	@NotBlank(message = UserLanguage.VALIDATION_NOT_EMPTY)
	private String dob;
	
	@Pattern(regexp = "^[0-9]{8,20}$", message = UserLanguage.VALIDATION_PHONE)
	private String phone;
	
	private String image;
	private String role;
	private byte status;
	private Date createdDate;
	private byte deleteFile = 0;
	private MultipartFile file;
	
	public User() {
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	public byte getGender() {
		return gender;
	}

	public void setGender(byte gender) {
		this.gender = gender;
	}	

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}	

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public byte getDeleteFile() {
		return deleteFile;
	}

	public void setDeleteFile(byte deleteFile) {
		this.deleteFile = deleteFile;
	}


	public MultipartFile getFile() {
		return file;
	}
	
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
}
