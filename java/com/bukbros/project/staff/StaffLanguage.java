package com.bukbros.project.staff;

import java.util.HashMap;

public class StaffLanguage {
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_SPECIAL_CHARACTERS = "Không được sử dụng ký tự đặc biệt";
	public static final String VALIDATION_IMAGE_SIZE = "Dung lượng file vượt quá 512 kB (0.5 MB)";	
	public static final String VALIDATION_IMAGE_UPLOAD = "Định dang file không hợp lệ";
	
	public static final String LABEL_NAME = "Tên";
	public static final String LABEL_USERNAME = "Tên truy cập";
	public static final String LABEL_FACULTY = "Khoa";
	public static final String LABEL_EMAIL = "Email";
	
	public static final String LABEL_CREATE = "Thêm mới nhân viên";
	public static final String LABEL_UPDATE = "Cập nhật nhân viên";
	public static final String LABEL_LIST = "Danh sách nhân viên";
	public static final String LABEL_DETAIL = "Chi tiết nhân viên";
	public static final String LABEL_CHANGE_PASSWORD = "Thay đổi mật khẩu";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_NAME", LABEL_NAME);
		labels.put("LABEL_USERNAME", LABEL_USERNAME);
		labels.put("LABEL_FACULTY", LABEL_FACULTY);
		labels.put("LABEL_EMAIL", LABEL_EMAIL);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		labels.put("LABEL_DETAIL", LABEL_DETAIL);
		labels.put("LABEL_CHANGE_PASSWORD", LABEL_CHANGE_PASSWORD);
		return labels;
	}
}
