package com.bukbros.project.course;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class CourseRepository implements RepositoryInterface<Course>{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean create(Course course) {
		String sql = "INSERT INTO " + CourseDatabase.TABLE
					+ " ("
					+ CourseDatabase.COLUMN_UUID+ ", "
					+ CourseDatabase.COLUMN_CODE+ ", "
					+ CourseDatabase.COLUMN_START_TIME+ ", "
					+ CourseDatabase.COLUMN_END_TIME					
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     course.getCode(),
		                                     DateValidation.formatDate(course.getStartTime()),
		                                     DateValidation.formatDate(course.getEndTime()));
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Course> read() {
		String sql = "SELECT " + CourseDatabase.COLUMN_ID + ", "
				   + "HEX(" + CourseDatabase.COLUMN_UUID + ") as " + CourseDatabase.COLUMN_UUID + ", "
				   + CourseDatabase.COLUMN_CODE + ", "
				   + CourseDatabase.COLUMN_START_TIME + ", "
				   + CourseDatabase.COLUMN_END_TIME
				   + " FROM " + CourseDatabase.TABLE 
				   + " ORDER BY " + CourseDatabase.COLUMN_ID + " ASC";
		List<Course> courses = jdbcTemplate.query(sql, new CourseRowMapper());
		if (courses.isEmpty()) {
			return null;
		}
		return courses;
	}

	@Override
	public boolean update(int id, Course object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean status(int id, int status) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected boolean isCodeExists(String code) {
		try {
			String sql = "SELECT " + CourseDatabase.COLUMN_CODE 
								   + " FROM " + CourseDatabase.TABLE
								   + " WHERE " + CourseDatabase.COLUMN_CODE + " = ?";
			String dbCode = jdbcTemplate.queryForObject(sql, new Object[] { code }, String.class);
			return code.equals(dbCode) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}

}
