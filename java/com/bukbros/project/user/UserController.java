package com.bukbros.project.user;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Role;
import com.bukbros.project.configuration.Url;
import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.staff.Staff;
import com.bukbros.project.staff.StaffService;
import com.bukbros.project.upload.UploadService;

@Controller
@RequestMapping(value = UserConfig.MODULE_ADMIN_URL)
public class UserController {
	@Autowired
	private UserService service;
	
	@Autowired
	private StaffService staffService;
	
	@Autowired
	private UploadService uploadService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return UserConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return UserLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showMenuUser(Model model, User user) {
		model.addAttribute("listMode", true);
		List<User> users = service.getAllUser();
		if (users == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("users", users);
		}
		return UserConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CREATE)
	public String showRegister(Model model) {
		model.addAttribute("createMode", true);
		return UserConfig.MODULE_VIEW_ADMIN;
	}

/*	@PostMapping(Url.URL_CREATE)
	public String register(	@ModelAttribute("validUser") @Valid User user,
							BindingResult results,
							Model model,
							RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("user", user);
		boolean validDate = DateValidation.isValid(user.getDob(), DateValidation.DATE_PATTERN);
		if (!validDate) {
			results.addError(new FieldError("validUser", "dob", UserLanguage.VALIDATION_DOB));
		}

		if (results.hasErrors()) {
			return UserConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean validUsername = service.isUsernameExists(user.getUsername());
			if (validUsername) {
				results.addError(new FieldError("validUser", "username", UserLanguage.VALIDATION_USERNAME));
			}
			boolean validEmail = service.isEmailExists(user.getEmail());
			if (validEmail) {
				results.addError(new FieldError("validUser", "email", UserLanguage.VALIDATION_EMAIL));
			}
			boolean confirmPassword = service.confirmPassword(user.getPassword(), user.getConfirmPassword());
			if (!confirmPassword) {
				results.addError(new FieldError("validUser",
												"confirmPassword",
												UserLanguage.VALIDATION_CONFIRM_PASS));
			}
			boolean maxDate = service.maxDate(user.getDob());
			if (maxDate) {
				results.addError(new FieldError("validUser", "dob", UserLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("username")	|| results.hasFieldErrors("email")
				|| results.hasFieldErrors("confirmPassword")
				|| results.hasFieldErrors("dob")) {
				return UserConfig.MODULE_VIEW_ADMIN;
			}
		}

		boolean createItem = service.create(request, user);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}*/

	@GetMapping(Url.URL_UPDATE)
	public String getUpdate(Principal principal, Model model) {
		User originalUser = service.getUserByUsername(principal.getName());
		if (originalUser == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("updateCurrentMode", true);
			model.addAttribute("user", originalUser);
		}
		return UserConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_UPDATE)
	public String postUpdate(	HttpServletRequest request,
								Principal principal,
								@ModelAttribute("validUser") @Valid User user,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {
		User originalUser = service.getUserByUsername(principal.getName());
		if (originalUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		model.addAttribute("updateCurrentMode", true);
		model.addAttribute("user", user);
		MultipartFile file = user.getFile();
		if (file.getSize() > 0) {
			if (!uploadService.validFileSize(file, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validUser", "file", UserLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(file, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validUser", "file", UserLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		}
		if (results.hasErrors()) {
			return UserConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!StringFunction.trimSpace(user.getEmail()).equalsIgnoreCase(originalUser.getEmail())) {
				boolean validEmail = service.isEmailExists(StringFunction.trimSpace(user.getEmail()));
				if (validEmail) {
					results.addError(new FieldError("validUser", "email", UserLanguage.VALIDATION_EMAIL));
				}
			}
			boolean maxDate = service.maxDate(user.getDob());
			if (maxDate) {
				results.addError(new FieldError("validUser", "dob", UserLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("email") || results.hasFieldErrors("dob")) {
				return UserConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(request, originalUser, user)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}

	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid,
							Principal principal,
							Model model,
							RedirectAttributes redirectAttributes) {
		User currentUser = service.getUserByUsername(principal.getName());
		User originalUser = service.getUserByUuid(uuid);
		if (originalUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			if (originalUser.getId() != currentUser.getId()) {
				redirectAttributes.addAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
				return "redirect:" + Url.URL_USER;
			}
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("user", originalUser);
		model.addAttribute("originalUser", originalUser);
		return UserConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(	@PathVariable("uuid") String uuid,
								HttpServletRequest request,
								Principal principal,
								@ModelAttribute("validUser") @Valid User user,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {
		User currentUser = service.getUserByUsername(principal.getName());
		User originalUser = service.getUserByUuid(uuid);
		if (originalUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			if (originalUser.getId() != currentUser.getId()) {
				redirectAttributes.addAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
				return "redirect:" + Url.URL_USER;
			}
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("user", user);
		model.addAttribute("originalUser", originalUser);
		MultipartFile file = user.getFile();
		if (file.getSize() > 0) {
			if (!uploadService.validFileSize(file, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validUser", "file", UserLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(file, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validUser", "file", UserLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		}
		if (results.hasErrors()) {
			return UserConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!StringFunction.trimSpace(user.getEmail()).equalsIgnoreCase(originalUser.getEmail())) {
				boolean validEmail = service.isEmailExists(StringFunction.trimSpace(user.getEmail()));
				if (validEmail) {
					results.addError(new FieldError("validUser", "email", UserLanguage.VALIDATION_EMAIL));
				}
			}
			boolean maxDate = service.maxDate(user.getDob());
			if (maxDate) {
				results.addError(new FieldError("validUser", "dob", UserLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("email") || results.hasFieldErrors("dob")) {
				return UserConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(request, originalUser, user)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}

	@GetMapping(Url.URL_DETAIL + "/{id}")
	public String getUserById(@PathVariable("id") int id, Model model) {
		Staff userById = staffService.getStaffById(id);
		if (userById == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("detailMode", true);
			model.addAttribute("user", userById);
		}
		return UserConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(Principal principal, Model model) {
		User originalUser = service.getUserByUsername(principal.getName());
		if (originalUser == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("changeCurrentPassMode", true);
			model.addAttribute("user", originalUser);
			model.addAttribute("originalUser", originalUser);
		}
		return UserConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(	Principal principal,
										@ModelAttribute("changePassword") @Valid User user,
										BindingResult results,
										Model model,
										RedirectAttributes redirectAttributes) {
		User userLoggin = service.getUserByUsername(principal.getName());
		if (userLoggin == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		model.addAttribute("changeCurrentPassMode", true);
		boolean checkPassword = service.checkPassword(user.getOldPassword(), userLoggin.getPassword());
		if (!checkPassword) {
			results.addError(new FieldError("changePassword", "oldPassword", UserLanguage.VALIDATION_PASS));
		}
		boolean confirmPassword = service.confirmPassword(user.getPassword(), user.getConfirmPassword());
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword",
											"confirmPassword",
											UserLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("password")	|| results.hasFieldErrors("oldPassword")
			|| results.hasFieldErrors("confirmPassword")) {
			return UserConfig.MODULE_VIEW_ADMIN;
		}
		if (service.changePassword(userLoggin.getId(), user)) {
			redirectAttributes.addFlashAttribute("successMessage", UserLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", UserLanguage.MESSAGE_CHANGE_PASS_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}
	
	@GetMapping("/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(@PathVariable("uuid") String uuid,
	                                Principal principal,
	                                Model model,
	                                RedirectAttributes redirectAttributes) {
		User currentUser = service.getUserByUsername(principal.getName());
		User originalUser = service.getUserByUuid(uuid);
		if (originalUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			if (originalUser.getId() != currentUser.getId()) {
				redirectAttributes.addAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
				return "redirect:" + Url.URL_USER;
			}
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("user", originalUser);
		model.addAttribute("originalUser", originalUser);
		return UserConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping("/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(	@PathVariable("uuid") String uuid,
	                                 	Principal principal,
										@ModelAttribute("changePassword") @Valid User user,
										BindingResult results,
										Model model,
										RedirectAttributes redirectAttributes) {
		User currentUser = service.getUserByUsername(principal.getName());
		User originalUser = service.getUserByUuid(uuid);
		if (originalUser == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			if (originalUser.getId() != currentUser.getId()) {
				redirectAttributes.addAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
				return "redirect:" + Url.URL_USER;
			}
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("user", originalUser);
		model.addAttribute("originalUser", originalUser);
		boolean checkPassword = service.checkPassword(user.getOldPassword(), currentUser.getPassword());
		if (!checkPassword) {
			results.addError(new FieldError("changePassword", "oldPassword", UserLanguage.VALIDATION_PASS));
		}
		boolean confirmPassword = service.confirmPassword(user.getPassword(), user.getConfirmPassword());
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword", "confirmPassword", UserLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("password")	|| results.hasFieldErrors("oldPassword")
			|| results.hasFieldErrors("confirmPassword")) {
			return UserConfig.MODULE_VIEW_ADMIN;
		}
		if (service.changePassword(currentUser.getId(), user)) {
			redirectAttributes.addFlashAttribute("successMessage", UserLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", UserLanguage.MESSAGE_CHANGE_PASS_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}

	@GetMapping(Url.URL_FORGOT_PASSWORD)
	public String getForgotPassword() {
		return "forgot-password";
	}

	@PostMapping(Url.URL_FORGOT_PASSWORD)
	public String postForgotPassword(	@ModelAttribute("forgotPassword") User user,
									BindingResult results,
									Model model,
									RedirectAttributes redirectAttributes) {
		model.addAttribute("forgotMode", true);
		User userByUsername = service.getUserByUsername(user.getUsername());
		boolean hasErrors = false;
		if (userByUsername == null) {
			hasErrors = true;
		} else {
			if (!userByUsername.getEmail().equals(user.getEmail())) {
				hasErrors = true;
			}
		}
		if (hasErrors) {
			model.addAttribute("errorMessage", UserLanguage.MESSAGE_FORGOT_PASSWORD_VALID);
			return "forgot-password";
		}
		user.setPassword(StringFunction.randomString(6));
		if (service.changePassword(userByUsername.getId(), userByUsername)) {
			redirectAttributes.addFlashAttribute("successMessage",	UserLanguage.MESSAGE_FORGOT_PASS_SUCCESS + user.getPassword());
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", UserLanguage.MESSAGE_FORGOT_PASS_FAILURE);
			return "forgot-password";
		}
		return "redirect:" + Url.URL_USER;
	}

	@PostMapping("/{uuid}" + Url.URL_ACTIVE)
	public String activeUser(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		User userByUuid = service.getUserByUuid(uuid);
		if (userByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		boolean active = service.status(userByUuid.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}

	@PostMapping("/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveUser(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		User userByUuid = service.getUserByUuid(uuid);
		if (userByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_USER;
		}
		boolean deactive = service.status(userByUuid.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_USER;
	}

}
