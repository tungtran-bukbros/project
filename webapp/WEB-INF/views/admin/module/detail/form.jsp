<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${urlGlobals.URL_TIME_TABLE}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${urlGlobals.URL_TIME_TABLE.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
			</a> <a href='<c:url value='${urlGlobals.URL_TIME_TABLE}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${urlGlobals.URL_TIME_TABLE.concat(urlGlobals.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm" value="${urlGlobals.URL_TIME_TABLE.concat('/').concat(detail.uuid).concat(urlGlobals.URL_UPDATE)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validDetail">
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Khóa học</label>
		<div class="col-sm-7">
			<select class="form-control" name="courseId" path="courseId">
				<c:forEach var="course" items="${courses}">
					<option value="${course.id}" ${course.id == detail.courseId ? 'selected' : ''}>${course.code}</option>
				</c:forEach>
			</select>
			<form:errors path="courseId" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Kỳ học</label>
		<div class="col-sm-7">
			<select class="form-control" name="term" path="term">
				<option value="1" selected>Kỳ 1</option>
				<option value="2" ${detail.term == 2 ? 'selected' : ''}>Kỳ 2</option>
				<option value="3" ${detail.term == 3 ? 'selected' : ''}>Kỳ 3</option>
			</select>
			<form:errors path="term" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Môn học</label>
		<div class="col-sm-7">
			<select class="form-control" name="subjectId" path="subjectId">
				<c:forEach var="subject" items="${subjects}">
					<option value="${subject.id}" ${subject.id == detail.subjectId ? 'selected' : ''}>${subject.title} (${subject.code})</option>
				</c:forEach>
			</select>
			<form:errors path="subjectId" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Lớp học</label>
		<div class="col-sm-7">
			<select class="form-control" name="classId" path="classId">
				<c:forEach var="classroom" items="${classrooms}">
					<option value="${classroom.id}" ${classroom.id == detail.classId ? 'selected' : ''}>${classroom.name}</option>
				</c:forEach>
			</select>
			<form:errors path="classId" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày bắt đầu</label>
		<div class="col-sm-7">
			<input type="text" class="form-control datepicker-top" name="startTime" value="${detail.startTime}" placeholder="Định dạng : ngày/tháng/năm"
				path="startTime">
			<form:errors path="startTime" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ngày kết thúc</label>
		<div class="col-sm-7">
			<input type="text" class="form-control datepicker-top" name="endTime" value="${detail.endTime}" placeholder="Định dạng : ngày/tháng/năm"
				path="endTime">
			<form:errors path="endTime" cssClass="error" />
		</div>
	</div>
	<hr />
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-9 offset-sm-3">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
				<hr>
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
