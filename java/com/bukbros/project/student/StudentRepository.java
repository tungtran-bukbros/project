package com.bukbros.project.student;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;
import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class StudentRepository implements RepositoryInterface<Student>{
	@Autowired
	private JdbcTemplate jdbcTemplate;	
	
	@Override
	public boolean create(Student student) {
		String sql = "INSERT INTO " + StudentDatabase.TABLE
					+ " ("
					+ StudentDatabase.COLUMN_UUID+ ", "
					+ StudentDatabase.COLUMN_USERNAME+ ", "
					+ StudentDatabase.COLUMN_PASSWORD+ ", "
					+ StudentDatabase.COLUMN_MAJOR_ID+ ", "
					+ StudentDatabase.COLUMN_EMAIL+ ", "
					+ StudentDatabase.COLUMN_FIRST_NAME+ ", "
					+ StudentDatabase.COLUMN_LAST_NAME+ ", "
					+ StudentDatabase.COLUMN_ADDRESS+ ", "
					+ StudentDatabase.COLUMN_DOB+ ", "
					+ StudentDatabase.COLUMN_GENDER+ ", "
					+ StudentDatabase.COLUMN_PHONE+ ", "
					+ StudentDatabase.COLUMN_STATUS
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?,?,?,?,?,?,?,?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     student.getUsername(),
		                                     StudentFunction.encoderPassword(StringFunction.trimSpace(student.getPassword())),
		                                     student.getMajorId(),
		                                     student.getEmail(),
		                                     student.getFirstName(),
		                                     student.getLastName(),
		                                     student.getAddress(),
		                                     DateValidation.formatDate(student.getDob()),
		                                     student.getGender(),
		                                     student.getPhone(),
		                                     student.getStatus());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Student> read() {
		String sql = "SELECT " + StudentDatabase.COLUMN_ID + ", "
							   + "HEX(" + StudentDatabase.COLUMN_UUID + ") as " + StudentDatabase.COLUMN_UUID + ", "
							   + StudentDatabase.COLUMN_USERNAME + ", "
							   + StudentDatabase.COLUMN_PASSWORD + ", "
							   + StudentDatabase.COLUMN_EMAIL + ", "
							   + StudentDatabase.COLUMN_FIRST_NAME + ", "
							   + StudentDatabase.COLUMN_LAST_NAME + ", "
							   + StudentDatabase.COLUMN_ADDRESS + ", "
							   + StudentDatabase.COLUMN_DOB + ", "
							   + StudentDatabase.COLUMN_GENDER + ", "
							   + StudentDatabase.COLUMN_PHONE + ", "
							   + StudentDatabase.COLUMN_STATUS + ", "
							   + StudentDatabase.COLUMN_CREATED_DATE
							   + " FROM " + StudentDatabase.TABLE 
							   + " ORDER BY " + StudentDatabase.COLUMN_ID + " ASC";
		List<Student> students = jdbcTemplate.query(sql, new StudentRowMapper());
		if (students.isEmpty()) {
			return null;
		}
		return students;
	}

	@Override
	public boolean update(int id, Student student) {
		String sql = "UPDATE " + StudentDatabase.TABLE
					+ " SET " + StudentDatabase.COLUMN_MAJOR_ID+ " = ?, "
					+ StudentDatabase.COLUMN_EMAIL+ " = ?, "
					+ StudentDatabase.COLUMN_FIRST_NAME+ " = ?, "
					+ StudentDatabase.COLUMN_LAST_NAME+ " = ?, "
					+ StudentDatabase.COLUMN_GENDER+ " = ?, "
					+ StudentDatabase.COLUMN_DOB+ " = ?, "
					+ StudentDatabase.COLUMN_PHONE+ " = ?, "
					+ StudentDatabase.COLUMN_ADDRESS+ " = ?, "
					+ StudentDatabase.COLUMN_STATUS+ " = ? "
					+ "WHERE "+ StudentDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                    student.getMajorId(),
		                                    StringFunction.trimSpace(student.getEmail()),
		                                    StringFunction.trimSpace(student.getFirstName()),
		                                    StringFunction.trimSpace(student.getLastName()),
		                                    student.getGender(),
		                                    DateValidation.formatDate(student.getDob()),
		                                    student.getPhone(),
		                                    StringFunction.trimSpace(student.getAddress()),
		                                    student.getStatus(), id);
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}


	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Student detail(int id) {
		Student student;
		try {
			String sql = "SELECT " + StudentDatabase.COLUMN_ID + ", "
					   + "HEX(" + StudentDatabase.COLUMN_UUID + ") as " + StudentDatabase.COLUMN_UUID + ", "
					   + StudentDatabase.COLUMN_USERNAME + ", "
					   + StudentDatabase.COLUMN_PASSWORD + ", "
					   + StudentDatabase.COLUMN_MAJOR_ID + ", "
					   + StudentDatabase.COLUMN_EMAIL + ", "
					   + StudentDatabase.COLUMN_FIRST_NAME + ", "
					   + StudentDatabase.COLUMN_LAST_NAME + ", "
					   + StudentDatabase.COLUMN_ADDRESS + ", "
					   + StudentDatabase.COLUMN_DOB + ", "
					   + StudentDatabase.COLUMN_GENDER + ", "
					   + StudentDatabase.COLUMN_PHONE + ", "
					   + StudentDatabase.COLUMN_STATUS + ", "
					   + StudentDatabase.COLUMN_CREATED_DATE
					   + " FROM " + StudentDatabase.TABLE 
					   + " WHERE " + StudentDatabase.COLUMN_ID + " = ? ";
			student = jdbcTemplate.queryForObject(sql, new Object[] { id }, new StudentRowMapper());
		} catch (Exception e) {
			return null;
		}
		return student;
	}

	@Override
	public boolean status(int id, int status) {
		try {
			String sql = "UPDATE " + StudentDatabase.TABLE 
		               + " SET " + StudentDatabase.COLUMN_STATUS + " = ? " 
		               + "WHERE " + StudentDatabase.COLUMN_ID + " = ? ";
			int rowActive = jdbcTemplate.update(sql, status, id);
			return (rowActive > 0) ? true : false;  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public List<Student> getAllStudent() {
		String sql = "SELECT " + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_ID + ", "
				   + "HEX(" + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_UUID + ") as " + StudentDatabase.COLUMN_UUID + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_USERNAME + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_PASSWORD + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_MAJOR_ID + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_EMAIL + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_FIRST_NAME + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_LAST_NAME + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_ADDRESS + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_DOB + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_GENDER + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_PHONE + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_STATUS + ", "
				   + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_CREATED_DATE + ", "
				   + StudentDatabase.TABLE_MAJOR + "." + StudentDatabase.TABLE_MAJOR_CODE + ", "
				   + StudentDatabase.TABLE_MAJOR + "." + StudentDatabase.TABLE_MAJOR_NAME
				   + " FROM " + StudentDatabase.TABLE 
				   + " INNER JOIN " + StudentDatabase.TABLE_MAJOR
				   + " ON " + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_MAJOR_ID + " = " + StudentDatabase.TABLE_MAJOR + "." + StudentDatabase.TABLE_MAJOR_ID
				   + " ORDER BY " + StudentDatabase.TABLE + "." + StudentDatabase.COLUMN_ID + " ASC";
		List<Student> students = jdbcTemplate.query(sql, new StudentFullRowMapper());
		if (students.isEmpty()) {
			return null;
		}
		return students;
	}
		
	protected boolean isUsernameExists(String username) {
		try {
			String sql = "SELECT " + StudentDatabase.COLUMN_USERNAME 
								   + " FROM " + StudentDatabase.TABLE
								   + " WHERE " + StudentDatabase.COLUMN_USERNAME + " = ?";
			String dbUsername = jdbcTemplate.queryForObject(sql, new Object[] { username }, String.class);
			return username.equals(dbUsername) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected boolean isEmailExists(String email) {
		try {
			String sql = "SELECT " + StudentDatabase.COLUMN_EMAIL 
								   + " FROM " + StudentDatabase.TABLE
								   + " WHERE " + StudentDatabase.COLUMN_EMAIL + " = ?";
			String dbEmail = jdbcTemplate.queryForObject(sql, new Object[] { email }, String.class);
			return email.equals(dbEmail) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected boolean changePassword(int id, Student student) {
		try {
			String sql = " UPDATE " + StudentDatabase.TABLE
					+ " SET "	+ StudentDatabase.COLUMN_PASSWORD+ " = ? "
					+ "WHERE "	+ StudentDatabase.COLUMN_ID+ " = ?";
			int rowActive = jdbcTemplate.update(sql, StudentFunction.encoderPassword(student.getPassword()), id);
			return (rowActive > 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected Student getStudentByUsername(String username) {
		Student student;
		try {
			String sql = " SELECT " + StudentDatabase.COLUMN_ID + ", "
							   + "HEX(" + StudentDatabase.COLUMN_UUID + ") as " + StudentDatabase.COLUMN_UUID+ ", "
							   + StudentDatabase.COLUMN_USERNAME + ", "
							   + StudentDatabase.COLUMN_PASSWORD + ", "
							   + StudentDatabase.COLUMN_MAJOR_ID + ", "
							   + StudentDatabase.COLUMN_EMAIL + ", "
							   + StudentDatabase.COLUMN_FIRST_NAME + ", "
							   + StudentDatabase.COLUMN_LAST_NAME + ", "
							   + StudentDatabase.COLUMN_ADDRESS + ", "
							   + StudentDatabase.COLUMN_DOB + ", "
							   + StudentDatabase.COLUMN_GENDER + ", "
							   + StudentDatabase.COLUMN_PHONE + ", "
							   + StudentDatabase.COLUMN_STATUS + ", "
							   + StudentDatabase.COLUMN_CREATED_DATE
							   + " FROM " + StudentDatabase.TABLE
							   + " WHERE "
						       +  StudentDatabase.COLUMN_USERNAME+ " = ? ";
			student = jdbcTemplate.queryForObject(sql, new Object[] { username }, new StudentRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return student;
	}
	
	protected Student getStudentByUuid (String uuid) {
		Student student;
		try {
			String sql = " SELECT " + StudentDatabase.COLUMN_ID + ", "
								+ "HEX(" + StudentDatabase.COLUMN_UUID + ") as " + StudentDatabase.COLUMN_UUID+ ", "
								+ StudentDatabase.COLUMN_USERNAME + ", "
							    + StudentDatabase.COLUMN_PASSWORD + ", "
							    + StudentDatabase.COLUMN_MAJOR_ID + ", "
							    + StudentDatabase.COLUMN_EMAIL + ", "
							    + StudentDatabase.COLUMN_FIRST_NAME + ", "
							    + StudentDatabase.COLUMN_LAST_NAME + ", "
							    + StudentDatabase.COLUMN_ADDRESS + ", "
							    + StudentDatabase.COLUMN_DOB + ", "
							    + StudentDatabase.COLUMN_GENDER + ", "
							    + StudentDatabase.COLUMN_PHONE + ", "
							    + StudentDatabase.COLUMN_STATUS + ", "
							    + StudentDatabase.COLUMN_CREATED_DATE
								+ " FROM " + StudentDatabase.TABLE
								+ " WHERE " + StudentDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			student = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new StudentRowMapper());
		} catch (Exception e) {
			return null;
		}
		return student;
	}

}
