package com.bukbros.project.student;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Url;
import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.major.MajorService;

@Controller
public class StudentController {
	@Autowired
	private StudentService service;
	@Autowired
	private MajorService majorService;

	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return StudentConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return StudentLanguage.label();
	}

	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}

	@GetMapping(Url.URL_STUDENT)
	public String getStudent(Model model, Student student) {
		model.addAttribute("listMode", true);
		List<Student> students = service.getAllStudent();
		if (students == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("students", students);
		}
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_STUDENT + Url.URL_CREATE)
	public String getRegister(Model model) {
		model.addAttribute("createMode", true);
		model.addAttribute("majors", majorService.getAllMajor());
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_STUDENT + Url.URL_CREATE)
	public String register(	@ModelAttribute("validStudent") @Valid Student student,
							BindingResult results,
							Model model,
							RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("student", student);
		boolean validDate = DateValidation.isValid(student.getDob(), DateValidation.DATE_PATTERN);
		if (!validDate) {
			results.addError(new FieldError("validStudent", "dob", StudentLanguage.VALIDATION_DOB));
		}

		if (results.hasErrors()) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean validUsername = service.isUsernameExists(student.getUsername());
			if (validUsername) {
				results.addError(new FieldError("validStudent", "username", StudentLanguage.VALIDATION_USERNAME));
			}
			boolean validEmail = service.isEmailExists(student.getEmail());
			if (validEmail) {
				results.addError(new FieldError("validStudent", "email", StudentLanguage.VALIDATION_EMAIL));
			}
			boolean confirmPassword = service.confirmPassword(student);
			if (!confirmPassword) {
				results.addError(new FieldError("validStudent",
												"confirmPassword",
												StudentLanguage.VALIDATION_CONFIRM_PASS));
			}
			boolean maxDate = service.maxDate(student.getDob());
			if (maxDate) {
				results.addError(new FieldError("validStudent", "dob", StudentLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("username")	|| results.hasFieldErrors("email")
				|| results.hasFieldErrors("confirmPassword")
				|| results.hasFieldErrors("dob")) {
				return StudentConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(student);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_STUDENT;
	}

	@GetMapping(Url.URL_STUDENT + "/{uuid}" + Url.URL_UPDATE)
	public String getUpdate(@PathVariable("uuid") String uuid, Model model, RedirectAttributes redirectAttributes) {
		Student originalStudent = service.getStudentByUuid(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("student", originalStudent);
		model.addAttribute("majors", majorService.getAllMajor());
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_STUDENT + "/{uuid}" + Url.URL_UPDATE)
	public String postUpdate(	@PathVariable("uuid") String uuid,
								@ModelAttribute("validStudent") @Valid Student student,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {
		Student originalStudent = service.getStudentByUuid(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("student", student);

		if (results.hasErrors()) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!StringFunction.trimSpace(student.getEmail()).equalsIgnoreCase(originalStudent.getEmail())) {
				boolean validEmail = service.isEmailExists(StringFunction.trimSpace(student.getEmail()));
				if (validEmail) {
					results.addError(new FieldError("validStudent", "email", StudentLanguage.VALIDATION_EMAIL));
				}
			}
			boolean maxDate = service.maxDate(student.getDob());
			if (maxDate) {
				results.addError(new FieldError("validStudent", "dob", StudentLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("email") || results.hasFieldErrors("dob")) {
				return StudentConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalStudent.getId(), student)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_STUDENT;
	}

	@GetMapping(Url.URL_STUDENT + Url.URL_DETAIL + "/{id}")
	public String getStudentById(@PathVariable("id") int id, Model model) {
		Student studentById = service.getStudentById(id);
		if (studentById == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		} else {
			model.addAttribute("detailMode", true);
			model.addAttribute("student", studentById);
			model.addAttribute("majors", majorService.getAllMajor());
		}
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@GetMapping(Url.URL_STUDENT + "/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String getChangePassword(@PathVariable("uuid") String uuid,
									Model model,
									RedirectAttributes redirectAttributes) {
		Student originalStudent = service.getStudentByUuid(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("student", originalStudent);
		return StudentConfig.MODULE_VIEW_ADMIN;
	}

	@PostMapping(Url.URL_STUDENT + "/{uuid}" + Url.URL_CHANGE_PASSWORD)
	public String postChangePassword(	@PathVariable("uuid") String uuid,
										@ModelAttribute("changePassword") @Valid Student student,
										BindingResult results,
										Model model,
										RedirectAttributes redirectAttributes) {
		Student originalStudent = service.getStudentByUuid(uuid);
		if (originalStudent == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		model.addAttribute("changePassMode", true);
		model.addAttribute("student", originalStudent);
		boolean checkPassword = service.checkPassword(student.getOldPassword(), originalStudent.getPassword());
		if (!checkPassword) {
			results.addError(new FieldError("changePassword", "oldPassword", StudentLanguage.VALIDATION_PASS));
		}
		boolean confirmPassword = service.confirmPassword(student);
		if (!confirmPassword) {
			results.addError(new FieldError("changePassword", "confirmPassword", StudentLanguage.VALIDATION_CONFIRM_PASS));
		}
		if (results.hasFieldErrors("password")	|| results.hasFieldErrors("oldPassword")
			|| results.hasFieldErrors("confirmPassword")) {
			return StudentConfig.MODULE_VIEW_ADMIN;
		}
		if (service.changePassword(originalStudent.getId(), student)) {
			redirectAttributes.addFlashAttribute("successMessage", StudentLanguage.MESSAGE_CHANGE_PASS_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", StudentLanguage.MESSAGE_CHANGE_PASS_FAILURE);
		}
		return "redirect:" + Url.URL_STUDENT;
	}

	@PostMapping(Url.URL_STUDENT + "/{uuid}" + Url.URL_ACTIVE)
	public String activeStudent(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Student studentByUuid = service.getStudentByUuid(uuid);
		if (studentByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		boolean active = service.status(studentByUuid.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_STUDENT;
	}

	@PostMapping(Url.URL_STUDENT + "/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveStudent(@PathVariable("uuid") String uuid,Model model, RedirectAttributes redirectAttributes) {
		Student studentByUuid = service.getStudentByUuid(uuid);
		if (studentByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STUDENT;
		}
		boolean deactive = service.status(studentByUuid.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_STUDENT;
	}
}
