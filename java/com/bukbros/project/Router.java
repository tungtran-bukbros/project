package com.bukbros.project;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Url;

@Controller
public class Router {
	
	@GetMapping("/")
	public String home(RedirectAttributes redirectAttributes) {
		return "redirect:" + Url.URL_LOGIN ;
	}	
	
	@GetMapping(Url.URL_LOGIN)
	public String login() {
		return "login";
	}
	
	@GetMapping(Url.URL_ADMIN)
	public String defaultSuccessUrl() {
		return "admin/module/dashboard";
	}
	
	@GetMapping(Url.URL_ERROR_NOT_FOUND)
	public String errorUrl() {
		return "error-not-found";
	}

}
