<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<body>
    <nav class="navbar navbar-expand-sm bg-light">
    	<div class="container">
    		<ul class="navbar-nav mr-auto">
		        <li class="nav-item">
		          <h4><a class="nav-link text-uppercase py-0" href='<c:url value="${urlGlobals.URL_ADMIN}"/>'>LOGO</a></h4>
		        </li>
	      	</ul>
	      <ul class="navbar-nav">
	      	<li class="nav-item dropdown">
	      		<c:choose>
				   	<c:when test="${not empty pageContext.request.userPrincipal}">
				   	<a class="btn btn-outline-danger dropdown-toggle home-nav" id="navbarDropdown" role="button" data-toggle="dropdown" href="#">${pageContext.request.userPrincipal.name}</a>
				   	</c:when>
	    		</c:choose>
	    		<div class="dropdown-menu" aria-labelledby="navbarDropdown">
	    			<a class="dropdown-item" href='<c:url value="${urlGlobals.URL_STAFF.concat(urlGlobals.URL_UPDATE)}"/>'><i class="fas fa-cog"></i> Tài khoản</a>
	    			<div class="dropdown-divider"></div>
	    			<a class="dropdown-item" href='<c:url value="${urlGlobals.URL_STAFF.concat(urlGlobals.URL_CHANGE_PASSWORD)}"/>'><i class="fas fa-unlock-alt"></i> Đổi mật khẩu</a>
	    			<div class="dropdown-divider"></div>
	    			<c:url value="${urlGlobals.URL_LOGOUT}" var="logout" />
	    			<form:form action="${logout}" method="post">	
		    			<div class="dropdown-item">
		    				<button type="submit" class="btn btn-link p-0 m-0 text-dark"><i class="fas fa-sign-out-alt"></i> Thoát</button>
		    			</div>					
					</form:form>
	    		</div>
	      	</li>
	      </ul>
    	</div>            
    </nav>
    <br>