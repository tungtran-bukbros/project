<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row mb-3">
	<div class="col-sm-3">
		<c:choose>
			<c:when test="${staff.image == null}">
				<img src="<c:url value="/public/images/avt_null.png" />" border="0" width="100%">
			</c:when>
			<c:otherwise>
				<img src="<c:url value="${staff.image}" />" border="0" width="100%">
			</c:otherwise>
		</c:choose>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Username : ${staff.username}</li>
			<li class="list-group-item">Họ tên : ${staff.firstName.concat(" ").concat(staff.lastName)}</li>
			<li class="list-group-item">Khoa : ${staff.facultyName}</li>
			<li class="list-group-item">Email : ${staff.email}</li>
			<li class="list-group-item">Địa chỉ : ${staff.address}</li>		
		</ul>
	</div>
	<div class="col-sm-4">
		<ul class="list-group list-group-flush">
			<li class="list-group-item">Ngày sinh : ${staff.dob}</li>	
			<li class="list-group-item">Giới tính : ${staff.gender == 1 ? 'Nam' : 'Nữ'}</li>
			<li class="list-group-item">Số điện thoại : ${staff.phone}</li>
			<li class="list-group-item">Chứng chỉ đạt được : ${staff.certificate}</li>
		</ul>
	</div>	
</div>
