package com.bukbros.project.language;

import java.util.HashMap;

public class MessageLanguage {
	public static final String MESSAGE_CREATE_SUCCESS = "Thêm mới dữ liệu thành công";
	public static final String MESSAGE_CREATE_FAILURE = "Thêm mới dữ liệu thất bại";
	
	public static final String MESSAGE_UPDATE_SUCCESS = "Cập nhật dữ liệu thành công";
	public static final String MESSAGE_UPDATE_FAILURE = "Cập nhật dữ liệu thất bại";
	
	public static final String MESSAGE_ACTIVE_SUCCESS = "Kích hoạt dữ liệu thành công";
	public static final String MESSAGE_ACTIVE_FAILURE = "Kích hoạt dữ liệu thất bại";
	
	public static final String MESSAGE_DEACTIVE_SUCCESS = "Khóa dữ liệu thành công";
	public static final String MESSAGE_DEACTIVE_FAILURE = "Khóa dữ liệu thất bại";
	
	public static final String MESSAGE_DELETE_SUCCESS = "Xóa dữ liệu thành công";
	public static final String MESSAGE_DELETE_FAILURE = "Xóa dữ liệu thất bại";
	
	public static final String MESSAGE_DATA_NOT_FOUND = "Không có dữ liệu được tìm thấy";
	public static final String MESSAGE_ROLE_FAILURE = "Dữ liệu không được phép truy cập";		
	
	public static HashMap<String, String> messageLanguage() {
		HashMap<String, String> messages = new HashMap<>();
		return messages;
	}
}
