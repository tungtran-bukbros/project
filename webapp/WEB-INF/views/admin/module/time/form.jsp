<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<c:if test="${createMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_CREATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${urlGlobals.URL_TIME}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
	<c:if test="${updateMode}">
		<div class="col-sm-6">
			<h5 class="text-uppercase mt-1 mb-0">
				<i class="fas fa-plus"></i> ${moduleLang.LABEL_UPDATE}
			</h5>
		</div>
		<div class="col-sm-6 text-right">
			<a href='<c:url value='${urlGlobals.URL_TIME.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
			</a> <a href='<c:url value='${urlGlobals.URL_TIME}' />' class="btn btn-info"><i class="fas fa-bars"></i> ${labelLang.LABEL_BUTTON_READ}</a>
		</div>
	</c:if>
</div>
<hr class="my-3">
<c:if test="${createMode}">
	<c:url var="urlForm" value="${urlGlobals.URL_TIME.concat(urlGlobals.URL_CREATE)}"></c:url>
</c:if>
<c:if test="${updateMode}">
	<c:url var="urlForm"
		value="${urlGlobals.URL_TIME.concat('/').concat(detail.uuid).concat(urlGlobals.URL_UPDATE).concat('?weekDay=').concat(param.weekDay)}"></c:url>
</c:if>
<form:form action="${urlForm}" method="post" modelAttribute="validTime">
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Thứ</label>
		<c:if test="${createMode}">
			<div class="col-sm-7">
				<select class="form-control" name="weekDay" path="weekDay">
					<c:forEach var="valueWeekDay" begin="2" end="7">
						<option value="${valueWeekDay}" ${param.weekDay == valueWeekDay ? 'selected' : ''}>Thứ ${valueWeekDay}</option>
					</c:forEach>
				</select>
				<form:errors path="weekDay" cssClass="error" />
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="col-sm-7">
				<c:forEach var="valueWeekDay" begin="2" end="7">
					<c:if test="${param.weekDay == valueWeekDay}">
						<p class="form-control">Thứ ${valueWeekDay}</p>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Ca</label>
		<div class="col-sm-7">
			<select class="form-control js-example-basic-multiple" name="blocks" path="blocks" multiple="multiple">
				<c:forEach var="valueBlock" begin="1" end="8">
					<c:forEach items="${time.blocks}" var="block">
						<c:if test="${block == valueBlock}">
							<c:set value="true" var="selected" />
						</c:if>
					</c:forEach>
					<option value="${valueBlock}" ${not empty selected ? ' selected' : '' }>Ca ${valueBlock}</option>
					<c:set value="" var="selected" />
				</c:forEach>
			</select>
			<form:errors path="blocks" cssClass="error" />
		</div>
	</div>
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right">Detail</label>
		<c:if test="${createMode}">
			<div class="col-sm-7">
				<select class="form-control" name="detailId" path="detailId">
					<c:forEach var="detail" items="${details}">
						<option value="${detail.id}" ${time.detailId == detail.id ? 'selected' : ''}>${detail.courseName} - Kỳ ${detail.term} - ${detail.subjectName} - ${detail.className}</option>
					</c:forEach>
				</select>
				<form:errors path="detailId" cssClass="error" />
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="col-sm-7">
				<c:forEach var="detailView" items="${details}">
					<c:if test="${detailView.id	 == detail.id}">
						<p class="form-control">${detailView.courseName} - Kỳ ${detailView.term} - ${detailView.subjectName} - ${detailView.className}</p>
					</c:if>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<hr />
	<div class="form-group row">
		<c:if test="${createMode}">
			<div class="col-sm-9 offset-sm-3">
				<button type="submit" class="btn btn-success">
					<i class="fas fa-plus"></i> ${labelLang.LABEL_CREATE}
				</button>
				<button type="reset" class="btn">
					<i class="fas fa-redo-alt"></i> ${labelLang.LABEL_RESET}
				</button>
			</div>
		</c:if>
		<c:if test="${updateMode}">
			<div class="offset-sm-3 col-sm-7">
				<button type="submit" class="btn btn-primary">
					<i class="fas fa-edit"></i> ${labelLang.LABEL_UPDATE}
				</button>
			</div>
		</c:if>
	</div>
</form:form>
