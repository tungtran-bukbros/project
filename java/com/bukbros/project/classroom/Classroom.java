package com.bukbros.project.classroom;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.springframework.stereotype.Component;

import com.bukbros.project.student.StudentLanguage;

@Component
public class Classroom {
	private int id;
	private String uuid;
	private String slug;
	
	@NotBlank(message = StudentLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[^!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = ClassroomLanguage.VALIDATION_SPECIAL_CHARACTERS)
	private String name;
	
	public Classroom() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
