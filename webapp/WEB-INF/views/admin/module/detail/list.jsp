<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value='${urlGlobals.URL_TIME_TABLE.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i>
		${labelLang.LABEL_BUTTON_CREATE}
		</a>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_COURSE_ID}</th>
			<th class="text-center">${moduleLang.LABEL_TERM}</th>
			<th class="text-center">${moduleLang.LABEL_SUBJECT_ID}</th>
			<th class="text-center">${moduleLang.LABEL_CLASS_ID}</th>
			<th class="text-center">${moduleLang.LABEL_START_TIME}</th>
			<th class="text-center">${moduleLang.LABEL_END_TIME}</th>
			<th class="text-center">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${details}" var="detail" varStatus="loop">
			<tr>
				<td class="text-center"></td>
				<td class="text-center">${detail.courseName}</td>
				<td class="text-center">${detail.term}</td>
				<td class="text-center">${detail.subjectName}</td>
				<td class="text-center">${detail.className}</td>
				<td class="text-center">${detail.startTime}</td>
				<td class="text-center">${detail.endTime}</td>
				<td class="text-center">
					<a href='<c:url value="${urlGlobals.URL_TIME_TABLE.concat('/').concat(detail.uuid).concat(urlGlobals.URL_UPDATE)}" />'
						class="btn btn-primary btn-sm"> ${labelLang.LABEL_BUTTON_UPDATE}</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>