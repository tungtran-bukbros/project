<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<title>Dang Nhap</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="description" content="" />
<meta name="author" content="" />
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- Datepicker CSS-->
<link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
</head>
<body>
	<div class="container-fluid p-0">
		<div class="container">
			<!-- Notify -->
			<c:if test="${not empty successMessage || not empty errorMessage}">
				<c:if test="${not empty successMessage}">
					<p class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Thành công!</strong> ${successMessage}
					</p>
				</c:if>
				<c:if test="${not empty errorMessage}">
					<p class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Thất bại!</strong> ${errorMessage}
					</p>
				</c:if>
			</c:if>
			<c:url value="/login" var="login_check"></c:url>
			<form:form modelAttribute="userlogin" class="form-signin" action="${login_check}" method="POST" autocomplete="off">
				<div class="row mt-3">
					<div class="col-sm-6 border-right"></div>
					<div class="col-sm-6">
						<div class="col-sm-12">
							<h5>
								<i class="fas fa-sign-in-alt"></i> Đăng nhập
							</h5>
							<hr class="my-2">
						</div>
						<div class="col-sm-12">
							<div class=" form-group">
								<label>Tên truy cập</label>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Username" name="username" path="username">
									<div class="input-group-prepend">
										<span class="input-group-text"> <i class="fas fa-user"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label>Mật khẩu</label>
								<div class="input-group">
									<input type="password" class="form-control" name="password" placeholder="Password" path="password">
									<div class="input-group-prepend">
										<span class="input-group-text"> <i class="fas fa-key"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group col-sm-12 text-right">
							<a href="<c:url value="/forgot-password" />">Quên mật khẩu?</a>
						</div>
						<div class="form-group col-sm-12 text-right">
							<button type="submit" class="btn btn-success">
								<i class="fas fa-sign-in-alt"></i> Đăng nhập
							</button>
							<button type="reset" class="btn">
								<i class="fas fa-redo-alt"></i> Làm lại
							</button>
						</div>
					</div>
				</div>
			</form:form>
			<div class="col-sm-12 text-center">
				<hr>
				Copyright © Bukbros 2018
			</div>
		</div>
	</div>
	<!--Bootstrap core js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<!--Datepicker js -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<!-- Fontawesome js -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</body>
</html>