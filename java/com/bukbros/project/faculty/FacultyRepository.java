package com.bukbros.project.faculty;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class FacultyRepository implements RepositoryInterface<Faculty>{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean create(Faculty faculty) {
		String sql = "INSERT INTO " + FacultyDatabase.TABLE
					+ " ("
					+ FacultyDatabase.COLUMN_UUID+ ", "
					+ FacultyDatabase.COLUMN_NAME+ ", "
					+ FacultyDatabase.COLUMN_STATUS
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     faculty.getName(),
		                                     faculty.getStatus());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Faculty> read() {
		String sql = "SELECT " + FacultyDatabase.COLUMN_ID + ", "
				   + "HEX(" + FacultyDatabase.COLUMN_UUID + ") as " + FacultyDatabase.COLUMN_UUID + ", "
				   + FacultyDatabase.COLUMN_NAME + ", "
				   + FacultyDatabase.COLUMN_STATUS
				   + " FROM " + FacultyDatabase.TABLE 
				   + " ORDER BY " + FacultyDatabase.COLUMN_ID + " ASC";
		List<Faculty> faculties = jdbcTemplate.query(sql, new FacultyRowMapper());
		if (faculties.isEmpty()) {
			return null;
		}
		return faculties;
	}

	@Override
	public boolean update(int id, Faculty faculty) {
		String sql = "UPDATE " + FacultyDatabase.TABLE
					+ " SET "  + FacultyDatabase.COLUMN_NAME+ " = ?, "
					+ FacultyDatabase.COLUMN_STATUS+ " = ? "
					+ " WHERE " + FacultyDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                     faculty.getName(),
		                                     faculty.getStatus(), id);
					
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = "DELETE FROM " + FacultyDatabase.TABLE + " WHERE " + FacultyDatabase.COLUMN_ID + " = ? ";
		int rowDeleted = jdbcTemplate.update(sql, id);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean status(int id, int status) {
		try {
			String sql = "UPDATE " + FacultyDatabase.TABLE 
		               + " SET " + FacultyDatabase.COLUMN_STATUS + " = ? " 
		               + "WHERE " + FacultyDatabase.COLUMN_ID + " = ? ";
			int rowActive = jdbcTemplate.update(sql, status, id);
			return (rowActive > 0) ? true : false;  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	protected Faculty getFacultyByUuid (String uuid) {
		Faculty faculty;
		try {
			String sql = " SELECT " + FacultyDatabase.COLUMN_ID + ", "
								+ "HEX(" + FacultyDatabase.COLUMN_UUID + ") as " + FacultyDatabase.COLUMN_UUID+ ", "
								+ FacultyDatabase.COLUMN_NAME + ", "
							    + FacultyDatabase.COLUMN_STATUS					    
								+ " FROM " + FacultyDatabase.TABLE
								+ " WHERE " + FacultyDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			faculty = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new FacultyRowMapper());
		} catch (Exception e) {
			return null;
		}
		return faculty;
	}
	
	protected boolean isNameExists(String name) {
		try {
			String sql = "SELECT " + FacultyDatabase.COLUMN_NAME 
								   + " FROM " + FacultyDatabase.TABLE
								   + " WHERE " + FacultyDatabase.COLUMN_NAME + " = ?";
			String dbCode = jdbcTemplate.queryForObject(sql, new Object[] { name }, String.class);
			return name.equals(dbCode) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
