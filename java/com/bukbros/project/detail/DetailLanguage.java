package com.bukbros.project.detail;

import java.util.HashMap;

public class DetailLanguage {
	public static final String VALIDATION_END_TIME = "Thời gian kết thúc không chính xác";
	public static final String VALIDATION_NOT_EMPTY = "Không được để trống";
	public static final String VALIDATION_DETAIL_EXISTS = "Môn học đã tồn tại trong kỳ này";
	
	public static final String LABEL_COURSE_ID = "Khóa học";
	public static final String LABEL_TERM = "Kỳ học";
	public static final String LABEL_SUBJECT_ID = "Môn học";
	public static final String LABEL_CLASS_ID = "Lớp học";
	public static final String LABEL_START_TIME = "Ngày bắt đầu";
	public static final String LABEL_END_TIME = "Ngày kết thúc";
	
	public static final String LABEL_CREATE = "Thêm mới buổi học";
	public static final String LABEL_UPDATE = "Cập nhật buổi học";
	public static final String LABEL_LIST = "Danh sách buổi học";
	
	public static HashMap<String, String> label() {
		HashMap<String, String> labels = new HashMap<>();
		labels.put("LABEL_COURSE_ID", LABEL_COURSE_ID);
		labels.put("LABEL_TERM", LABEL_TERM);
		labels.put("LABEL_SUBJECT_ID", LABEL_SUBJECT_ID);
		labels.put("LABEL_CLASS_ID", LABEL_CLASS_ID);
		labels.put("LABEL_START_TIME", LABEL_START_TIME);
		labels.put("LABEL_END_TIME", LABEL_END_TIME);
		
		labels.put("LABEL_CREATE", LABEL_CREATE);
		labels.put("LABEL_UPDATE", LABEL_UPDATE);
		labels.put("LABEL_LIST", LABEL_LIST);
		
		return labels;
	}
}
