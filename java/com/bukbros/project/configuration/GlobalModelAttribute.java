package com.bukbros.project.configuration;

import java.security.Principal;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.bukbros.project.user.User;
import com.bukbros.project.user.UserService;

@ControllerAdvice
public class GlobalModelAttribute {
	@Autowired
	private UserService userService;
	
	@ModelAttribute("role")
	public HashMap<String, String> roles() {
		return Role.roles();
	}
	
	@ModelAttribute("currentUser")
	public User getCurrntUser(Principal principal) {
		if (principal != null) {
			return userService.getUserByUsername(principal.getName());
		}
			return null;
	}
}
