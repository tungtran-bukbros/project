package com.bukbros.project.classroom;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.StringFunction;
import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class ClassroomRepository implements RepositoryInterface<Classroom>{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean create(Classroom classroom) {
		String sql = "INSERT INTO " + ClassroomDatabase.TABLE
					+ " ("
					+ ClassroomDatabase.COLUMN_UUID+ ", "
					+ ClassroomDatabase.COLUMN_SLUG+ ", "
					+ ClassroomDatabase.COLUMN_NAME
					+ ") VALUES (UNHEX(REPLACE(?, \"-\",\"\")),?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     UUID.randomUUID().toString(),
		                                     classroom.getSlug().trim(),
		                                     classroom.getName());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Classroom> read() {
		String sql = "SELECT " + ClassroomDatabase.COLUMN_ID + ", "
				   + "HEX(" + ClassroomDatabase.COLUMN_UUID + ") as " + ClassroomDatabase.COLUMN_UUID + ", "
				   + ClassroomDatabase.COLUMN_SLUG + ", "
				   + ClassroomDatabase.COLUMN_NAME
				   + " FROM " + ClassroomDatabase.TABLE 
				   + " ORDER BY " + ClassroomDatabase.COLUMN_ID + " ASC";
		List<Classroom> classrooms = jdbcTemplate.query(sql, new ClassroomRowMapper());
		if (classrooms.isEmpty()) {
			return null;
		}
		return classrooms;
	}

	@Override
	public boolean update(int id, Classroom classroom) {
		String sql = "UPDATE " + ClassroomDatabase.TABLE
					+ " SET " + ClassroomDatabase.COLUMN_NAME+ " = ? "
					+ " WHERE " + ClassroomDatabase.COLUMN_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql, StringFunction.trimSpace(classroom.getName()), id);
					
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		String sql = "DELETE FROM " + ClassroomDatabase.TABLE + " WHERE " + ClassroomDatabase.COLUMN_ID + " = ? ";
		int rowDeleted = jdbcTemplate.update(sql, id);
		if (rowDeleted > 0) {
			return true;
		}
		return false;
	}

	@Override
	public Object detail(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean status(int id, int status) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected Classroom getClassroomByUuid (String uuid) {
		Classroom classroom;
		try {
			String sql = " SELECT " + ClassroomDatabase.COLUMN_ID + ", "
								+ "HEX(" + ClassroomDatabase.COLUMN_UUID + ") as " + ClassroomDatabase.COLUMN_UUID+ ", "
								+ ClassroomDatabase.COLUMN_SLUG + ", "
							    + ClassroomDatabase.COLUMN_NAME					    
								+ " FROM " + ClassroomDatabase.TABLE
								+ " WHERE " + ClassroomDatabase.COLUMN_UUID + " = UNHEX(REPLACE(?, \"-\",\"\"))";
			classroom = jdbcTemplate.queryForObject(sql, new Object[] { uuid.trim() }, new ClassroomRowMapper());
		} catch (Exception e) {
			return null;
		}
		return classroom;
	}

	
	protected boolean isSlugExists(String slug) {
		try {
			String sql = "SELECT " + ClassroomDatabase.COLUMN_SLUG 
								   + " FROM " + ClassroomDatabase.TABLE
								   + " WHERE " + ClassroomDatabase.COLUMN_SLUG + " = ?";
			String dbSlug = jdbcTemplate.queryForObject(sql, new Object[] { slug }, String.class);
			return slug.equals(dbSlug) ? true : false;
		} catch (Exception e) {
			return false;
		}
	}
	
	protected boolean addClassSubject (int subjectId, int classId) {
		try {
			String sql =" INSERT INTO" + ClassroomDatabase.TABLE_CLASS_SUBJECT
						+ "( "
						+ ClassroomDatabase.COLUMN_CLASS_ID+ ", "
						+ ClassroomDatabase.COLUMN_SUBJECT_ID
						+ ") VALUES (?,?)";
			int rowCreated = jdbcTemplate.update(sql, subjectId, classId);
			return (rowCreated) > 0 ? true : false;
		} catch (Exception e) {
			return false;
		} 
	}

}
