package com.bukbros.project.student;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Component
public class Student {
	private int id;
	private String uuid;
	private int majorId;
	
	@Size(min = 4, max = 30, message = StudentLanguage.VALIDATION_SIZE_MIN_4_MAX_30)
	@Pattern(regexp = "[a-zA-Z0-9]+", message = StudentLanguage.VALIDATION_REGEX_a_A_0)
	private String username;
	
	@Size(min = 6, max = 30, message = StudentLanguage.VALIDATION_SIZE_MIN_6_MAX_30)
	@Pattern(regexp = "[^ ]+", message = StudentLanguage.VALIDATION_REGEX_NOT_SPACE)
	private String password;
	
	private String confirmPassword;
	
	private String oldPassword;
	
	@NotBlank(message = StudentLanguage.VALIDATION_NOT_EMPTY)
	@Email(message = StudentLanguage.VALIDATION_EMAIL)
	private String email;
	
	@NotBlank(message = StudentLanguage.VALIDATION_NOT_EMPTY)
	@Size(max = 50, message = StudentLanguage.VALIDATION_MAX_50)
	@Pattern(regexp = "[^0-9_+-.,!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = StudentLanguage.VALIDATION_FIRSTNAME)
	private String firstName;
	
	@NotBlank(message = StudentLanguage.VALIDATION_NOT_EMPTY)
	@Size(max = 50, message = StudentLanguage.VALIDATION_MAX_50)
	@Pattern(regexp = "[^0-9_+-.,!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = StudentLanguage.VALIDATION_LASTNAME)
	private String lastName;
	
	@NotBlank(message = StudentLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[^!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = StudentLanguage.VALIDATION_SPECIAL_CHARACTERS)
	private String address;
	
	@NotBlank(message = StudentLanguage.VALIDATION_NOT_EMPTY)
	private String dob;
	
	private byte gender;
	
	@Pattern(regexp = "[0-9 ]+", message = StudentLanguage.VALIDATION_PHONE)
	private String phone;
	
	private byte status;
	private Date createdDate;
	private String majorCode;
	private String majorName;
	
	public Student() {		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public byte getGender() {
		return gender;
	}

	public void setGender(byte gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getMajorId() {
		return majorId;
	}

	public void setMajorId(int majorId) {
		this.majorId = majorId;
	}

	public String getMajorCode() {
		return majorCode;
	}

	public void setMajorCode(String majorCode) {
		this.majorCode = majorCode;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	
}
