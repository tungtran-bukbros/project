package com.bukbros.project.detail;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.classroom.Classroom;
import com.bukbros.project.classroom.ClassroomService;
import com.bukbros.project.configuration.Url;
import com.bukbros.project.course.Course;
import com.bukbros.project.course.CourseService;
import com.bukbros.project.function.DateValidation;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.subject.Subject;
import com.bukbros.project.subject.SubjectService;

@Controller
@RequestMapping(value = DetailConfig.MODULE_ADMIN_URL)
public class DetailController {
	@Autowired
	private DetailService service;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private SubjectService subjectService;
	
	@Autowired
	private ClassroomService classService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return DetailConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return DetailLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getDetail(Model model) {
		model.addAttribute("listMode", true);
		List<Detail> details = service.getAllDetail();
		if (details == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("details", details);
		}
		return DetailConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (Model model) {
		List<Course> courses = courseService.getAllCourse();
		List<Subject> subjects = subjectService.getAllSubject();
		List<Classroom> classrooms = classService.getAllClassroom();
		model.addAttribute("createMode", true);
		model.addAttribute("courses", courses);
		model.addAttribute("subjects", subjects);
		model.addAttribute("classrooms", classrooms);
		return DetailConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validDetail") @Valid Detail detail,
							  BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		List<Course> courses = courseService.getAllCourse();
		List<Subject> subjects = subjectService.getAllSubject();
		List<Classroom> classrooms = classService.getAllClassroom();		
		model.addAttribute("createMode", true);
		model.addAttribute("detail", detail);
		model.addAttribute("courses", courses);
		model.addAttribute("subjects", subjects);
		model.addAttribute("classrooms", classrooms);
		if  (results.hasErrors()) {
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		Detail detailExists = service.isDetailExists(detail.getCourseId(), detail.getTerm(), detail.getSubjectId(), detail.getClassId());
		if (detailExists != null) {
			model.addAttribute("errorMessage", DetailLanguage.VALIDATION_DETAIL_EXISTS);
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		if (DateValidation.convertStringToDate(detail.getStartTime()).after(DateValidation.convertStringToDate(detail.getEndTime()))) {
			results.addError(new FieldError("validDetail", "endTime", DetailLanguage.VALIDATION_END_TIME));
		}
		if (results.hasFieldErrors("endTime")) {
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		
		boolean createItem = service.create(detail);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_TIME_TABLE;
	}
	
	@GetMapping("/{uuid}" + Url.URL_UPDATE)
	public String getUpdate (@PathVariable("uuid") String uuid, Model model, RedirectAttributes redirectAttributes) {
		Detail originalDetail = service.getDetailByUuid(uuid);
		List<Course> courses = courseService.getAllCourse();
		List<Subject> subjects = subjectService.getAllSubject();
		List<Classroom> classrooms = classService.getAllClassroom();
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME_TABLE;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("detail", originalDetail);
		model.addAttribute("courses", courses);
		model.addAttribute("subjects", subjects);
		model.addAttribute("classrooms", classrooms);
		return DetailConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping("/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
							  @ModelAttribute("validDetail") @Valid Detail detail,
							  BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		Detail originalDetail = service.getDetailByUuid(uuid);
		List<Course> courses = courseService.getAllCourse();
		List<Subject> subjects = subjectService.getAllSubject();
		List<Classroom> classrooms = classService.getAllClassroom();
		if (originalDetail == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_TIME_TABLE;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("detail", detail);
		model.addAttribute("courses", courses);
		model.addAttribute("subjects", subjects);
		model.addAttribute("classrooms", classrooms);
		if  (results.hasErrors()) {
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		Detail detailExists = service.isDetailExists(detail.getCourseId(), detail.getTerm(), detail.getSubjectId(), detail.getClassId());
		if (detailExists != null) {
			model.addAttribute("errorMessage", DetailLanguage.VALIDATION_DETAIL_EXISTS);
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		if (DateValidation.convertStringToDate(detail.getStartTime()).after(DateValidation.convertStringToDate(detail.getEndTime()))) {
			results.addError(new FieldError("validDetail", "endTime", DetailLanguage.VALIDATION_END_TIME));
		}
		if (results.hasFieldErrors("endTime")) {
			return DetailConfig.MODULE_VIEW_ADMIN;
		}
		if (service.update(originalDetail.getId(), detail)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_TIME_TABLE;
	}
}
