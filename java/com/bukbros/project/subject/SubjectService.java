package com.bukbros.project.subject;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubjectService {
	@Autowired
	private SubjectRepository repository;
	
	public List<Subject> getAllSubject() {
		return repository.read();
	}
	
	public boolean create(Subject subject) {
		return repository.create(subject);
	}
	
	public boolean update(int id, Subject subject) {
		return repository.update(id, subject);
	}
	
	public boolean delete(Subject subject) {
		return repository.delete(subject.getId());
	}
	
	public boolean status(int id, int status) {
		return repository.status(id, status);
	}
	
	protected boolean isTitleExists(String title) {
		return repository.isTitleExists(title);
	}
	
	protected boolean isCodeExists(String code) {
		return repository.isCodeExists(code);
	}
	
	protected Subject getSubjectByUuid (String uuid) {
		return repository.getSubjectByUuid(uuid);
	}
	
}
