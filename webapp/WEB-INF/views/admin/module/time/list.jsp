<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0">
			<i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}
		</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value='${urlGlobals.URL_TIME.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
		</a>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_WEEK_DAY}</th>
			<th class="text-center">${moduleLang.LABEL_BLOCK}</th>
			<th class="text-center">${moduleLang.LABEL_DETAIL_ID}</th>
			<th class="text-center">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${groups}" var="group">
			<tr ${group.status == 0 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">${group.weekDay}</td>
				<td class="text-center">
					<c:forEach items="${times}" var="time" varStatus="loop">
						<c:if test="${(group.detailId == time.detailId) && (group.weekDay == time.weekDay)}">
							<c:choose>
								<c:when test="${time.block == 1}">
									<c:set var="colorBlock" value="badge-primary" />
								</c:when>
								<c:when test="${time.block == 2}">
									<c:set var="colorBlock" value="badge-secondary" />
								</c:when>
								<c:when test="${time.block == 3}">
									<c:set var="colorBlock" value="badge-success" />
								</c:when>
								<c:when test="${time.block == 4}">
									<c:set var="colorBlock" value="badge-danger" />
								</c:when>
								<c:when test="${time.block == 5}">
									<c:set var="colorBlock" value="badge-warning" />
								</c:when>
								<c:when test="${time.block == 6}">
									<c:set var="colorBlock" value="badge-info" />
								</c:when>
								<c:when test="${time.block == 7}">
									<c:set var="colorBlock" value="badge-dark" />
								</c:when>
								<c:when test="${time.block == 8}">
									<c:set var="colorBlock" value="badge-light" />
								</c:when>
							</c:choose>
							<span class="badge badge-pill ${colorBlock}">${time.block}</span>${!loop.last ? ' ' : ''}
						</c:if>
					</c:forEach>
				</td>
				<c:forEach items="${details}" var="detail">
					<c:if test="${group.detailId == detail.id}">
						<td class="text-center">${detail.courseName}- Kỳ ${detail.term} - ${detail.subjectName} - ${detail.className}</td>
						<c:set var="detailUuid" value="${detail.uuid}" />
					</c:if>
				</c:forEach>
				<td class="text-center">				
				<c:choose>
					<c:when test="${group.status == 1}">
						<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_DEACTIVE}" />
						<c:set var="btnClass" value="btn-secondary"/>
						<c:url value="${urlGlobals.URL_TIME.concat('/').concat(detailUuid).concat(urlGlobals.URL_DEACTIVE).concat('?weekDay=').concat(group.weekDay)}" var="actionLink" />
					</c:when>
					<c:otherwise>
						<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_ACTIVE}" />
						<c:set var="btnClass" value="btn-warning"/>
						<c:url value="${urlGlobals.URL_TIME.concat('/').concat(detailUuid).concat(urlGlobals.URL_ACTIVE).concat('?weekDay=').concat(group.weekDay)}" var="actionLink" />
					</c:otherwise>
				</c:choose>
					<form:form action="${actionLink}" method="post">
						<a href='<c:url value="${urlGlobals.URL_TIME.concat('/').concat(detailUuid).concat(urlGlobals.URL_UPDATE).concat('?weekDay=').concat(group.weekDay)}" />' class="btn btn-primary btn-sm"> ${labelLang.LABEL_BUTTON_UPDATE} </a>
						<button type="submit" class="btn ${btnClass} btn-sm"> ${activeLabel}</button>
					</form:form>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>