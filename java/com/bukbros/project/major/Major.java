package com.bukbros.project.major;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.springframework.stereotype.Component;

import com.bukbros.project.function.StringFunction;

@Component
public class Major {
	private int id;
	private String uuid;
	
	@NotBlank(message = MajorLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[^0-9!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = MajorLanguage.VALIDATION_SPECIAL_CHARACTERS)
	private String name;
	
	@NotBlank(message = MajorLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[^0-9!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = MajorLanguage.VALIDATION_SPECIAL_CHARACTERS)
	private String code;
	
	private byte status;
	
	public Major() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = StringFunction.trimSpace(name);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = StringFunction.trimSpace(code);
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
}
