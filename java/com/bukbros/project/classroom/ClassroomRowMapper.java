package com.bukbros.project.classroom;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.StringFunction;

public class ClassroomRowMapper implements RowMapper<Classroom>{
	public Classroom mapRow(ResultSet rs, int rowNo) throws SQLException {
		Classroom classroom = new Classroom();
		classroom.setId(rs.getInt(ClassroomDatabase.COLUMN_ID));
		classroom.setUuid(StringFunction.convertTextToUuid(rs.getString(ClassroomDatabase.COLUMN_UUID)));
		classroom.setSlug(rs.getString(ClassroomDatabase.COLUMN_SLUG));
		classroom.setName(rs.getString(ClassroomDatabase.COLUMN_NAME));
		return classroom;
	}

}
