package com.bukbros.project.staff;

public class StaffDatabase {
	public static final String TABLE = "cr_staff";
	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_FACULTY_ID = "faculty_id";
	public static final String COLUMN_CERTIFICATE = "certificate";
	
	public static final String TABLE_USER = "cr_user";
	public static final String TABLE_USER_ID = "id";
	public static final String TABLE_USER_UUID = "uuid";
	public static final String TABLE_USER_USERNAME = "username";
	public static final String TABLE_USER_PASSWORD = "password";
	public static final String TABLE_USER_EMAIL = "email";
	public static final String TABLE_USER_FIRST_NAME = "first_name";
	public static final String TABLE_USER_LAST_NAME = "last_name";
	public static final String TABLE_USER_ADDRESS = "address";
	public static final String TABLE_USER_GENDER = "gender";
	public static final String TABLE_USER_DOB = "dob";
	public static final String TABLE_USER_PHONE = "phone";
	public static final String TABLE_USER_IMAGE = "image";
	public static final String TABLE_USER_ROLE = "role";
	public static final String TABLE_USER_STATUS = "status";
	public static final String TABLE_USER_CREATED_DATE = "created_date";
	
	public static final String TABLE_FACULTY = "cr_faculty";
	public static final String TABLE_FACULTY_ID = "id";
	public static final String TABLE_FACULTY_NAME = "name";
}
