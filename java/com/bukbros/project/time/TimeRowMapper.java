package com.bukbros.project.time;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TimeRowMapper implements RowMapper<Time>{
	public Time mapRow(ResultSet rs, int rowNo) throws SQLException {
		Time time = new Time();
		time.setDetailId(rs.getInt(TimeDatabase.COLUMN_DETAIL_ID));
		time.setWeekDay(rs.getString(TimeDatabase.COLUMN_WEEK_DAY));
		time.setBlock(rs.getString(TimeDatabase.COLUMN_BLOCK));
		time.setStatus(rs.getByte(TimeDatabase.COLUMN_STATUS));
		return time;
	}

}
