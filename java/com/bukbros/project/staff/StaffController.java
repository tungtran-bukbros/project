package com.bukbros.project.staff;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Url;
import com.bukbros.project.faculty.FacultyService;
import com.bukbros.project.function.DateValidation;
import com.bukbros.project.function.StringFunction;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.upload.UploadService;
import com.bukbros.project.user.UserConfig;
import com.bukbros.project.user.UserLanguage;
import com.bukbros.project.user.UserService;

@Controller
@RequestMapping(value = StaffConfig.MODULE_ADMIN_URL)
public class StaffController {
	@Autowired
	private StaffService service;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private FacultyService facultyService;
	
	@Autowired
	private UploadService uploadService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return StaffConfig.component();
	}

	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return StaffLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getStaff(Model model) {
		model.addAttribute("listMode", true);
		List<Staff> staffs = service.getAllStaff();
		if (staffs == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("staffs", staffs);
		}
		return StaffConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String showRegister(Model model) {
		model.addAttribute("createMode", true);
		model.addAttribute("faculties", facultyService.getAllFaculty());
		return StaffConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String register(	@ModelAttribute("validStaff") @Valid Staff staff,
	                       	HttpServletRequest request,
							BindingResult results,
							Model model,
							RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("staff", staff);
		model.addAttribute("faculties", facultyService.getAllFaculty());
		boolean validDate = DateValidation.isValid(staff.getDob(), DateValidation.DATE_PATTERN);
		if (!validDate) {
			results.addError(new FieldError("validStaff", "dob", UserLanguage.VALIDATION_DOB));
		}
		MultipartFile file = staff.getFile();
		if (file.getSize() > 0) {
			if (!uploadService.validFileSize(file, UserConfig.MAX_IMAGE_FILE_SIZE)) {
				results.addError(new FieldError("validStaff", "file", StaffLanguage.VALIDATION_IMAGE_SIZE));
			}
			if (!(uploadService.validFileExt(file, UserConfig.EXT_IMAGE_FILE_UPLOAD))) {
				results.addError(new FieldError("validStaff", "file", StaffLanguage.VALIDATION_IMAGE_UPLOAD));
			}
		} else {
			results.addError(new FieldError("validStaff", "file", StaffLanguage.VALIDATION_NOT_EMPTY));
		}

		if (results.hasErrors()) {
			return StaffConfig.MODULE_VIEW_ADMIN;
		} else {
			boolean validUsername = userService.isUsernameExists(staff.getUsername());
			if (validUsername) {
				results.addError(new FieldError("validStaff", "username", UserLanguage.VALIDATION_USERNAME));
			}
			boolean validEmail = userService.isEmailExists(staff.getEmail());
			if (validEmail) {
				results.addError(new FieldError("validStaff", "email", UserLanguage.VALIDATION_EMAIL));
			}
			boolean confirmPassword = userService.confirmPassword(staff.getPassword(), staff.getConfirmPassword());
			if (!confirmPassword) {
				results.addError(new FieldError("validStaff",
												"confirmPassword",
												UserLanguage.VALIDATION_CONFIRM_PASS));
			}			
			boolean maxDate = userService.maxDate(staff.getDob());
			if (maxDate) {
				results.addError(new FieldError("validStaff", "dob", UserLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("username")	|| results.hasFieldErrors("email")
				|| results.hasFieldErrors("confirmPassword")
				|| results.hasFieldErrors("dob")) {
				return StaffConfig.MODULE_VIEW_ADMIN;
			}
		}

		boolean createItem = service.create(request, staff);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_STAFF;
	}
	
	@GetMapping(Url.URL_UPDATE)
	public String getUpdate(Principal principal, Model model) {
		Staff originalStaff = service.getStaffByUsername(principal.getName());
		if (originalStaff == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("updateCurrentMode", true);
			model.addAttribute("faculties", facultyService.getAllFaculty());
			model.addAttribute("staff", originalStaff);
		}
		return StaffConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_UPDATE)
	public String postUpdate(	HttpServletRequest request,
								Principal principal,
								@ModelAttribute("validStaff") @Valid Staff staff,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {
		Staff originalStaff = service.getStaffByUsername(principal.getName());
		if (originalStaff == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_STAFF;
		}
		model.addAttribute("updateCurrentMode", true);
		model.addAttribute("faculties", facultyService.getAllFaculty());
		model.addAttribute("staff", staff);
		boolean validDob = DateValidation.isValid(staff.getDob(), DateValidation.DATE_PATTERN);
		if (!validDob) {
			results.addError(new FieldError("validStaff", "dob", UserLanguage.VALIDATION_DOB));
		}
		if (results.hasErrors()) {
			return StaffConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!StringFunction.trimSpace(staff.getEmail()).equalsIgnoreCase(originalStaff.getEmail())) {
				boolean validEmail = userService.isEmailExists(StringFunction.trimSpace(staff.getEmail()));
				if (validEmail) {
					results.addError(new FieldError("validStaff", "email", UserLanguage.VALIDATION_EMAIL));
				}
			}
			boolean maxDate = userService.maxDate(staff.getDob());
			if (maxDate) {
				results.addError(new FieldError("validStaff", "dob", UserLanguage.VALIDATION_DOB));
			}
			if (results.hasFieldErrors("email") || results.hasFieldErrors("dob")) {
				return StaffConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(request, originalStaff, staff)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_STAFF;
	}
	@GetMapping(Url.URL_DETAIL + "/{id}")
	public String getStaffById(@PathVariable("id") int id, Model model) {
		Staff staffById = service.getStaffById(id);
		if (staffById == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("detailMode", true);
			model.addAttribute("staff", staffById);
		}
		return StaffConfig.MODULE_VIEW_ADMIN;
	}
}
