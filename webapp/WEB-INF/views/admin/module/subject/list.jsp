<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="row">
	<div class="col-sm-6">
		<h5 class="text-uppercase mt-1 mb-0"><i class="fas fa-list-ul"></i> ${moduleLang.LABEL_LIST}</h5>
	</div>
	<div class="col-sm-6 text-right">
		<a href='<c:url value='${urlGlobals.URL_SUBJECT.concat(urlGlobals.URL_CREATE)}' />' class="btn btn-success"> <i
			class="fas fa-plus"></i> ${labelLang.LABEL_BUTTON_CREATE}
		</a>
	</div>
</div>
<hr class="my-3">
<table id="datatable" class="table table-hover">
	<thead>
		<tr>
			<th class="text-center">${labelLang.LABEL_STT}</th>
			<th class="text-center">${moduleLang.LABEL_TITLE}</th>
			<th class="text-center">${moduleLang.LABEL_CODE}</th>
			<th class="text-center">${moduleLang.LABEL_CREADIT}</th>
			<th class="text-center">${labelLang.LABEL_OPTION}</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${subjects}" var="subject" varStatus="loop">
			<tr ${subject.status == 0 ? 'class="bg-light"' : ''}>
				<td class="text-center"></td>
				<td class="text-center">${subject.title}</td>
				<td class="text-center">${subject.code}</td>
				<td class="text-center">${subject.creadit}</td>
				<td class="text-center">		
				<c:choose>
					<c:when test="${subject.status == 1}">
						<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_DEACTIVE}" />
						<c:set var="btnClass" value="btn-secondary"/>
						<c:url value="${urlGlobals.URL_SUBJECT.concat('/').concat(subject.uuid).concat(urlGlobals.URL_DEACTIVE)}" var="actionLink" />
					</c:when>
					<c:otherwise>
						<c:set var="activeLabel" value="${labelLang.LABEL_BUTTON_ACTIVE}" />
						<c:set var="btnClass" value="btn-warning"/>
						<c:url value="${urlGlobals.URL_SUBJECT.concat('/').concat(subject.uuid).concat(urlGlobals.URL_ACTIVE)}" var="actionLink" />
					</c:otherwise>
				</c:choose>			
					<form:form action="${actionLink}" method="post">
						 <a href='<c:url value="${urlGlobals.URL_SUBJECT.concat('/').concat(subject.uuid).concat(urlGlobals.URL_UPDATE)}" />' class="btn btn-primary btn-sm">
							${labelLang.LABEL_BUTTON_UPDATE} </a>
						<button type="submit" class="btn ${btnClass} btn-sm"> ${activeLabel}</button>
					</form:form>					
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>