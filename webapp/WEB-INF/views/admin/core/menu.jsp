<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
			<div class="list-group mb-3">
				<a href='<c:url value="${urlGlobals.URL_ADMIN}" />' class="list-group-item list-group-item-action dashboard-link">Dashboard</a>
				<a href='<c:url value="${urlGlobals.URL_USER}" />' class="list-group-item list-group-item-action sub-item">Thành viên</a>
				<a href='<c:url value="${urlGlobals.URL_STAFF}" />' class="list-group-item list-group-item-action sub-item">Nhân viên</a>
				<a href='<c:url value="${urlGlobals.URL_STUDENT}" />' class="list-group-item list-group-item-action sub-item">Sinh viên</a>
				<a href='<c:url value="${urlGlobals.URL_FACULTY}" />' class="list-group-item list-group-item-action sub-item">Khoa</a>
				<a href='<c:url value="${urlGlobals.URL_MAJOR}" />' class="list-group-item list-group-item-action sub-item">Ngành học</a>
				<a href='<c:url value="${urlGlobals.URL_SUBJECT}" />' class="list-group-item list-group-item-action sub-item">Môn học</a>
				<a href='<c:url value="${urlGlobals.URL_CLASSROOM}" />' class="list-group-item list-group-item-action sub-item">Lớp học</a>
				<a href='<c:url value="${urlGlobals.URL_COURSE}" />' class="list-group-item list-group-item-action sub-item">Khóa học</a>
				<a href='<c:url value="${urlGlobals.URL_TIME_TABLE}" />' class="list-group-item list-group-item-action sub-item">Detail</a>
				<a href='<c:url value="${urlGlobals.URL_TIME}" />' class="list-group-item list-group-item-action sub-item">Thời khóa biểu</a>
			</div>
		</div>
		<div class="col-sm-10">