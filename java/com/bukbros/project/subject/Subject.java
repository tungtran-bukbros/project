package com.bukbros.project.subject;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.springframework.stereotype.Component;

import com.bukbros.project.function.StringFunction;

@Component
public class Subject {
	private int id;
	private String uuid ;
	
	@NotBlank(message = SubjectLanguage.VALIDATION_NOT_EMPTY)
	private String title;
	
	@NotBlank(message = SubjectLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[^!@~`#$?%^&*():;\\[\\]{}\\\\/|<>\"']+", message = SubjectLanguage.VALIDATION_SPECIAL_CHARACTERS)
	private String code;
	
	@NotBlank(message = SubjectLanguage.VALIDATION_NOT_EMPTY)
	@Pattern(regexp = "[0-9]",message = SubjectLanguage.VALIDATION_ONLY_NUMBER)
	private String creadit;
	
	private byte status;
	
	public Subject() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = StringFunction.trimSpace(title);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = StringFunction.trimSpace(code);
	}	

	public String getCreadit() {
		return creadit;
	}

	public void setCreadit(String creadit) {
		this.creadit = creadit;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	
}
