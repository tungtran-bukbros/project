package com.bukbros.project.staff;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.bukbros.project.function.StringFunction;
import com.bukbros.project.interfaces.RepositoryInterface;

@Repository
public class StaffRepository implements RepositoryInterface<Staff>{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean create(Staff staff) {
		String sql = "INSERT INTO " + StaffDatabase.TABLE
					+ " ("
					+ StaffDatabase.COLUMN_USER_ID+ ", "
					+ StaffDatabase.COLUMN_FACULTY_ID+ ", "
					+ StaffDatabase.COLUMN_CERTIFICATE			
					+ ") VALUES (?,?,?)";
		int rowCreated = jdbcTemplate.update(sql,
		                                     staff.getUserId(),
		                                     staff.getFacultyId(),
		                                     staff.getCertificate());
		if (rowCreated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Staff> read() {
		String sql = "SELECT " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID + ", "
				   + "HEX("  + StaffDatabase.TABLE_USER+ "." + StaffDatabase.TABLE_USER_UUID + ") as " + StaffDatabase.TABLE_USER_UUID + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_USERNAME + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PASSWORD + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_EMAIL + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_FIRST_NAME + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_LAST_NAME + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ADDRESS + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_GENDER + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_DOB + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PHONE + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_IMAGE + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_STATUS + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ROLE + ", "
				   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_CREATED_DATE + ", "
				   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + ", "
				   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID + ", "
				   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_CERTIFICATE + ", "
				   + StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_NAME
				   + " FROM " + StaffDatabase.TABLE
				   + " INNER JOIN " + StaffDatabase.TABLE_USER
				   + " ON " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + " = " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID
				   + " INNER JOIN " + StaffDatabase.TABLE_FACULTY
				   + " ON " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID + " = " + StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_ID
				   + " ORDER BY " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + " ASC";
		List<Staff> staffs = jdbcTemplate.query(sql, new StaffFullRowMapper());
		if (staffs.isEmpty()) {
		return null;
		}
		return staffs;
	}

	@Override
	public boolean update(int id, Staff staff) {
		String sql = "UPDATE " + StaffDatabase.TABLE
				+ " SET " + StaffDatabase.COLUMN_FACULTY_ID+ " = ?, "
				+ StaffDatabase.COLUMN_CERTIFICATE+ " = ? "		
				+ "WHERE "+ StaffDatabase.COLUMN_USER_ID+ " = ? ";
		int rowUpdated = jdbcTemplate.update(sql,
		                                     staff.getFacultyId(),
		                                     StringFunction.trimSpace(staff.getCertificate()), id);
		if (rowUpdated > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Staff detail(int id) {
		Staff staff;
		try {
			String sql = "SELECT " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID + ", "
					   + "HEX("  + StaffDatabase.TABLE_USER+ "." + StaffDatabase.TABLE_USER_UUID + ") as " + StaffDatabase.TABLE_USER_UUID + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_USERNAME + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PASSWORD + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_EMAIL + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_FIRST_NAME + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_LAST_NAME + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ADDRESS + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_GENDER + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_DOB + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PHONE + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_IMAGE + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_STATUS + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ROLE + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_CREATED_DATE + ", "
					   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + ", "
					   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID + ", "
					   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_CERTIFICATE + ", "
					   + StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_NAME
					   + " FROM " + StaffDatabase.TABLE
					   + " INNER JOIN " + StaffDatabase.TABLE_USER
					   + " ON " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + " = " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID
					   + " INNER JOIN " + StaffDatabase.TABLE_FACULTY
					   + " ON " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID + " = " + StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_ID
					   + " WHERE " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + " = ? ";
			 staff = jdbcTemplate.queryForObject(sql, new Object[] { id }, new StaffFullRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return staff;
	}

	@Override
	public boolean status(int id, int status) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected Staff getStaffByUsername (String username) {
		Staff staff;
		try {
			String sql = "SELECT " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID + ", "
					   + "HEX("  + StaffDatabase.TABLE_USER+ "." + StaffDatabase.TABLE_USER_UUID + ") as " + StaffDatabase.TABLE_USER_UUID + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_USERNAME + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PASSWORD + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_EMAIL + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_FIRST_NAME + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_LAST_NAME + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ADDRESS + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_GENDER + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_DOB + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_PHONE + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_IMAGE + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_STATUS + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ROLE + ", "
					   + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_CREATED_DATE + ", "
					   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + ", "
					   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID + ", "
					   + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_CERTIFICATE + ", "
					   + StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_NAME
					   + " FROM " + StaffDatabase.TABLE
					   + " INNER JOIN " + StaffDatabase.TABLE_USER
					   + " ON " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_USER_ID + " = " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_ID
					   + " INNER JOIN " + StaffDatabase.TABLE_FACULTY
					   + " ON " + StaffDatabase.TABLE + "." + StaffDatabase.COLUMN_FACULTY_ID + " = " + StaffDatabase.TABLE_FACULTY + "." + StaffDatabase.TABLE_FACULTY_ID
					   + " WHERE " + StaffDatabase.TABLE_USER + "." + StaffDatabase.TABLE_USER_USERNAME + " = ? ";
			 staff = jdbcTemplate.queryForObject(sql, new Object[] { username }, new StaffFullRowMapper());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return staff;
	}

}
