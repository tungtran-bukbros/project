package com.bukbros.project.major;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.bukbros.project.function.StringFunction;

public class MajorRowMapper implements RowMapper<Major>{
	public Major mapRow(ResultSet rs, int rowNo) throws SQLException {
		Major major = new Major();
		major.setId(rs.getInt(MajorDatabase.COLUMN_ID));
		major.setUuid(StringFunction.convertTextToUuid(rs.getString(MajorDatabase.COLUMN_UUID)));
		major.setName(rs.getString(MajorDatabase.COLUMN_NAME));
		major.setCode(rs.getString(MajorDatabase.COLUMN_CODE));
		major.setStatus(rs.getByte(MajorDatabase.COLUMN_STATUS));
		return major;
	}
	
}
