package com.bukbros.project.subject;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Url;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;

@Controller
public class SubjectController {
	@Autowired
	private SubjectService service;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return SubjectConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return SubjectLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}
	
	@GetMapping(Url.URL_SUBJECT)
	public String getSubject(Model model, Subject subject) {
		model.addAttribute("listMode", true);
		List<Subject> subjects = service.getAllSubject();
		if (subjects == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("subjects", subjects);
		}
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_SUBJECT + Url.URL_CREATE)
	public String getCreate (Model model) {
		model.addAttribute("createMode", true);
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_SUBJECT + Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validSubject") @Valid Subject subject,
								BindingResult results,
								Model model,
								RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("subject", subject);
		if (results.hasErrors()) {
			return SubjectConfig.MODULE_VIEW_ADMIN;
		}else {
			boolean validTitle = service.isTitleExists(subject.getTitle());
			if(validTitle) {
				results.addError(new FieldError("validSubject", "title", SubjectLanguage.VALIDATION_TITLE));
			}
			boolean validCode = service.isCodeExists(subject.getCode());
			if(validCode) {
				results.addError(new FieldError("validSubject", "code", SubjectLanguage.VALIDATION_CODE));
			}
			if (results.hasFieldErrors("title")	|| results.hasFieldErrors("code")){
				return SubjectConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(subject);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}
	
	@GetMapping(Url.URL_SUBJECT + "/{uuid}" + Url.URL_UPDATE)
	public String getUpdate (@PathVariable("uuid") String uuid,
	                         Model model,
	                         RedirectAttributes redirectAttributes) {
		Subject originalSubject = service.getSubjectByUuid(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("subject", originalSubject);
		return SubjectConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_SUBJECT + "/{uuid}" + Url.URL_UPDATE)
	public String postUpdate (@PathVariable("uuid") String uuid,
	                          @ModelAttribute("validSubject") @Valid Subject subject,
	                          BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		Subject originalSubject = service.getSubjectByUuid(uuid);
		if (originalSubject == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		model.addAttribute("updateMode", true);
		model.addAttribute("subject", subject);
		if (results.hasErrors()) {
			return SubjectConfig.MODULE_VIEW_ADMIN;
		} else {
			if (!subject.getTitle().equalsIgnoreCase(originalSubject.getTitle())) {
				boolean validTitle = service.isTitleExists(subject.getTitle());
				if(validTitle) {
					results.addError(new FieldError("validSubject", "title", SubjectLanguage.VALIDATION_TITLE));
				}
			}
			if (!subject.getCode().equalsIgnoreCase(originalSubject.getCode())) {
				boolean validCode = service.isCodeExists(subject.getCode());
				if(validCode) {
					results.addError(new FieldError("validSubject", "code", SubjectLanguage.VALIDATION_CODE));
				}
			}
			if (results.hasFieldErrors("title") || results.hasFieldErrors("code")) {
				return SubjectConfig.MODULE_VIEW_ADMIN;
			}
		}
		if (service.update(originalSubject.getId(), subject)) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_UPDATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_UPDATE_FAILURE);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}
	
	@PostMapping(Url.URL_SUBJECT + "/{uuid}" + Url.URL_ACTIVE)
	public String activeSubject(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Subject subjectByUuid = service.getSubjectByUuid(uuid);
		if (subjectByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		boolean active = service.status(subjectByUuid.getId(), 1);
		if (active) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_ACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}

	@PostMapping(Url.URL_SUBJECT + "/{uuid}" + Url.URL_DEACTIVE)
	public String deactiveSubject(@PathVariable("uuid") String uuid, RedirectAttributes redirectAttributes) {
		Subject subjectByUuid = service.getSubjectByUuid(uuid);
		if (subjectByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		boolean deactive = service.status(subjectByUuid.getId(), 0);
		if (deactive) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DEACTIVE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DEACTIVE_FAILURE);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}
	
	@PostMapping(Url.URL_SUBJECT + "/{uuid}" + Url.URL_DELETE )
	public String deleteSubject(@PathVariable("uuid") String uuid,
	                            Model model,
	                            RedirectAttributes redirectAttributes) {
		Subject subjectByUuid = service.getSubjectByUuid(uuid);
		if (subjectByUuid == null) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
			return "redirect:" + Url.URL_SUBJECT;
		}
		boolean delete = service.delete(subjectByUuid);
		if (delete) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_DELETE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_DELETE_FAILURE);
		}
		return "redirect:" + Url.URL_SUBJECT;
	}	
}
