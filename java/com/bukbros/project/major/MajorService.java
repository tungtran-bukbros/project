package com.bukbros.project.major;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MajorService {
	@Autowired
	private MajorRepository repository;
	
	public List<Major> getAllMajor() {
		return repository.read();
	}
	
	public boolean create(Major major) {
		return repository.create(major);
	}
	
	public boolean update(int id, Major major) {
		return repository.update(id, major);
	}
	
	public boolean status(int id, int status) {
		return repository.status(id, status);
	}
	
	public boolean delete(Major major) {
		return repository.delete(major.getId());
	}
	
	public Major getMajorByUuid (String uuid) {
		return repository.getMajorByUuid(uuid);
	}
	
	public boolean isCodeExists(String code) {
		return repository.isCodeExists(code);
	}
	
	public boolean isNameExists(String name) {
		return repository.isNameExists(name);
	}
}
