package com.bukbros.project.configuration;

import java.util.HashMap;

import com.bukbros.project.classroom.ClassroomConfig;
import com.bukbros.project.course.CourseConfig;
import com.bukbros.project.detail.DetailConfig;
import com.bukbros.project.faculty.FacultyConfig;
import com.bukbros.project.major.MajorConfig;
import com.bukbros.project.staff.StaffConfig;
import com.bukbros.project.student.StudentConfig;
import com.bukbros.project.subject.SubjectConfig;
import com.bukbros.project.time.TimeConfig;
import com.bukbros.project.user.UserConfig;

public class Url {

	public static final String URL_ADMIN = "/admin";
	public static final String URL_LOGIN = "/login";
	public static final String URL_LOGOUT = "/logout";
	public static final String URL_REGISTER = "/register";
	public static final String URL_FORGOT_PASSWORD = "/forgot-password";
	public static final String URL_CHANGE_PASSWORD = "/change-password";
	public static final String URL_ERROR_NOT_FOUND = "/error-not-found";	
	
	public static final String URL_MENU_HOME = "/home";
	public static final String URL_USER = UserConfig.MODULE_ADMIN_URL;
	public static final String URL_STAFF = StaffConfig.MODULE_ADMIN_URL;
	public static final String URL_STUDENT = StudentConfig.MODULE_ADMIN_URL; 
	public static final String URL_SUBJECT = SubjectConfig.MODULE_ADMIN_URL;
	public static final String URL_CLASSROOM = ClassroomConfig.MODULE_ADMIN_URL;
	public static final String URL_COURSE = CourseConfig.MODULE_ADMIN_URL;
	public static final String URL_TIME_TABLE = DetailConfig.MODULE_ADMIN_URL;
	public static final String URL_TIME = TimeConfig.MODULE_ADMIN_URL;
	public static final String URL_MAJOR = MajorConfig.MODULE_ADMIN_URL;
	public static final String URL_FACULTY = FacultyConfig.MODULE_ADMIN_URL;
	
	public static final String URL_CREATE = "/create";
	public static final String URL_UPDATE = "/update";
	public static final String URL_DELETE = "/delete";	
	public static final String URL_ACTIVE = "/active";
	public static final String URL_DEACTIVE = "/deactive";
	public static final String URL_DETAIL = "/detail";
	
	public static HashMap<String, String> urlGlobal() {
		HashMap<String, String> urlGlobals = new HashMap<>();
		urlGlobals.put("URL_ADMIN", URL_ADMIN);
		urlGlobals.put("URL_LOGIN", URL_LOGIN);
		urlGlobals.put("URL_LOGOUT", URL_LOGOUT);
		urlGlobals.put("URL_REGISTER", URL_REGISTER);
		urlGlobals.put("URL_FORGOT_PASSWORD", URL_FORGOT_PASSWORD);
		urlGlobals.put("URL_CHANGE_PASSWORD", URL_CHANGE_PASSWORD);
		urlGlobals.put("URL_ERROR_NOT_FOUND", URL_ERROR_NOT_FOUND);
		
		urlGlobals.put("URL_MENU_HOME", URL_MENU_HOME);
		urlGlobals.put("URL_USER", URL_USER);	
		urlGlobals.put("URL_STAFF", URL_STAFF);	
		urlGlobals.put("URL_STUDENT", URL_STUDENT);	
		urlGlobals.put("URL_SUBJECT", URL_SUBJECT);	
		urlGlobals.put("URL_CLASSROOM", URL_CLASSROOM);
		urlGlobals.put("URL_COURSE", URL_COURSE);
		urlGlobals.put("URL_TIME_TABLE", URL_TIME_TABLE);
		urlGlobals.put("URL_TIME", URL_TIME);
		urlGlobals.put("URL_MAJOR", URL_MAJOR);
		urlGlobals.put("URL_FACULTY", URL_FACULTY);
		
		urlGlobals.put("URL_CREATE", URL_CREATE);
		urlGlobals.put("URL_UPDATE", URL_UPDATE);
		urlGlobals.put("URL_DELETE", URL_DELETE);		
		urlGlobals.put("URL_ACTIVE", URL_ACTIVE);
		urlGlobals.put("URL_DEACTIVE", URL_DEACTIVE);
		urlGlobals.put("URL_DETAIL", URL_DETAIL);
		return urlGlobals;
	}
}
