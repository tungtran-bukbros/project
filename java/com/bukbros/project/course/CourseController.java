package com.bukbros.project.course;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bukbros.project.configuration.Role;
import com.bukbros.project.configuration.Url;
import com.bukbros.project.function.DateValidation;
import com.bukbros.project.language.LabelLanguage;
import com.bukbros.project.language.MessageLanguage;
import com.bukbros.project.user.User;
import com.bukbros.project.user.UserService;

@Controller
@RequestMapping(value = CourseConfig.MODULE_ADMIN_URL)
public class CourseController {
	@Autowired
	private CourseService service;
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute("module")
	public HashMap<String, String> module() {
		return CourseConfig.component();
	}
	
	@ModelAttribute("moduleLang")
	public HashMap<String, String> moduleLang() {
		return CourseLanguage.label();
	}
	
	@ModelAttribute("labelLang")
	public HashMap<String, String> labelLanguage() {
		return LabelLanguage.labelLanguage();
	}	
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAllCourse(Model model,
	                           Principal principal){
		model.addAttribute("listMode", true);
		User currentUser = userService.getUserByUsername(principal.getName());
		List<Course> courses = service.getAllCourse();
		if (courses == null) {
			model.addAttribute("errorMessage", MessageLanguage.MESSAGE_DATA_NOT_FOUND);
		} else {
			model.addAttribute("staff", currentUser);
			model.addAttribute("courses", courses);
		}
		return CourseConfig.MODULE_VIEW_ADMIN;
	}
	
	@GetMapping(Url.URL_CREATE)
	public String getCreate (Model model,
	                         Principal principal,
							 RedirectAttributes redirectAttributes) {
		User currentUser = userService.getUserByUsername(principal.getName());
		if (!currentUser.getRole().equalsIgnoreCase(Role.ROLE_ADMIN)) {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_ROLE_FAILURE);
			return "redirect:" + Url.URL_COURSE;
		}else {
			model.addAttribute("createMode", true);
		}
		return CourseConfig.MODULE_VIEW_ADMIN;
	}
	
	@PostMapping(Url.URL_CREATE)
	public String postCreate (@ModelAttribute("validCourse") @Valid Course course,
							  BindingResult results,
							  Model model,
							  RedirectAttributes redirectAttributes) {
		model.addAttribute("createMode", true);
		model.addAttribute("course", course);
		if (results.hasErrors()) {
			return CourseConfig.MODULE_VIEW_ADMIN;
		}else {
			boolean validCode = service.isCodeExists(course.getCode());
			if (validCode) {
				results.addError(new FieldError("validCourse", "code", CourseLanguage.VALIDATION_CODE));
			}
			if (DateValidation.convertStringToDate(course.getStartTime()).after(DateValidation.convertStringToDate(course.getEndTime()))) {
				results.addError(new FieldError("validCourse", "endTime", CourseLanguage.VALIDATION_END_TIME));
			}			
			if (results.hasFieldErrors("code")	|| results.hasFieldErrors("endTime")) {
				return CourseConfig.MODULE_VIEW_ADMIN;
			}
		}
		boolean createItem = service.create(course);
		if (createItem) {
			redirectAttributes.addFlashAttribute("successMessage", MessageLanguage.MESSAGE_CREATE_SUCCESS);
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", MessageLanguage.MESSAGE_CREATE_FAILURE);
		}
		return "redirect:" + Url.URL_COURSE;
	}

}
